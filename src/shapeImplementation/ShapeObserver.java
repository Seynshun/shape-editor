package shapeImplementation;

public interface ShapeObserver {
    
    void update(Shape shape);
}