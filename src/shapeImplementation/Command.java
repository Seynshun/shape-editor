package shapeImplementation;

public interface Command {

    public void execute();
    public String getName();
}
