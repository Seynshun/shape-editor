package shapeImplementation;

import history.Snapshot;

public class History {
    Command command;
    Snapshot snapshot;
    History(Command c, Snapshot m) {
        command = c;
        snapshot = m;
    }

    public Command getCommand() {
        return command;
    }

    public Snapshot getSnapshot() {
        return snapshot;
    }
    
}
