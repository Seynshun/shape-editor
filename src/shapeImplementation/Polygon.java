package shapeImplementation;
import editors.PolygonEditor;

/**
 * Class representing a regular polygon and its properties
 */

public class Polygon extends BasedShape{

    private double width;
    private int numberOfSides;
    private Editor editor;
    private double reductionCoefficient;
    private double virtualWidth;


    public Polygon(String name,Position position,Color color, int numberOfSides,double width){
        super(name, position,color,Math.PI*180/Math.PI);
        setNumberOfSides(numberOfSides);
        setWidth(width);
        setCoordinates();
        this.editor=PolygonEditor.getInstance();
        virtualWidth = width;
    }

    /**
     * {@inheritDoc}
     */
    public void draw(Controller controller) {
        setCoordinates();
        controller.drawPolygon(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInside(double x, double y) {
        double somme = 0.0;
        Position pointPosition = new Position(x,y);
        setCoordinates();
        for(int i = 0; i<coordinates.length; i++){
            if(i == coordinates.length -1){
                
                double tmp = triangleArea(coordinates[i], coordinates[0],pointPosition);
                if (tmp == 0){
                    return true;
                }
                somme += tmp;
            }
            else{

                double tmp = triangleArea(coordinates[i], coordinates[i+1], pointPosition);
                somme += tmp;
            }

        }
        //find total area
        double totalArea = triangleArea(coordinates[0], coordinates[1], position) * numberOfSides;

        
        return ( Math.abs( somme - totalArea) <1 );
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void setCoordinates() {
       this.coordinates= new Position[numberOfSides];
       double angle = this.angle*Math.PI/180;
       final double angleStep = Math.PI*2/numberOfSides;
       for (int i = 0; i < numberOfSides; i++, angle += angleStep) {
           double x = Math.sin(angle) * width + position.getX();
           double y = Math.cos(angle) * width + position.getY();
           this.coordinates[i]= new Position(x,y);
       }

    }

    private double triangleArea(Position p1, Position p2, Position p3){
        double a = p1.getDistance(p2);
        double b = p1.getDistance(p3);
        double c = p2.getDistance(p3);
        double p = (a+b+c)/2;
        double result = Math.sqrt( p * (p-a) * (p-b) * (p-c));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(Visitor v){
        v.visitPolygon(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Shape clone(){
        Polygon s = null;
        s = (Polygon) super.clone();
        s.setReductionCoefficient(reductionCoefficient);
        s.setVirtualWidth(virtualWidth);
        s.rotate(angle);
        return s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void edit() {
       editor.initialisation(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rotate(double rotation){
        this.angle = rotation;
         
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getSize(){
        /**
         * On récupère le plus petit Y et le plus grand, et on renvoie la longueur qui les sépare.
         */
        double y_min= coordinates[0].getY();
        double y_max =coordinates[0].getY() ;


        for(int i = 0; i < numberOfSides; i ++){
            double point = coordinates[i].getY();
            if(point <=  y_min ){
                y_min = point;
            }
            else if(point >= y_max){
                y_max = point;
            }
        }
        
        return Math.abs(y_max - y_min);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPosition(Position pos){
        this.position = pos;
         
        setCoordinates();
    }

    /**
     * Reduces the size of the polygon
     * @param coefficient of reduction
     */
    @Override
    public void reduce(double coefficient){
        if(reductionCoefficient !=0){
            setCoordinates();
            return;
        }
        
        virtualWidth = width;
        reductionCoefficient = coefficient;
        width = width * coefficient;
    
        setCoordinates();
    }

    /**
     * Resize the polygon
     */
    @Override
    public void resize(){
        width = virtualWidth;
        setCoordinates();
        reductionCoefficient = 0;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setSize(double coefficient) {
        setWidth(this.width + this.width * coefficient/100);
        setCoordinates();
    }

    public void setNumberOfSides(int numberOfSides){
        this.numberOfSides= numberOfSides;
        setCoordinates();
    }
    public double getWidth() {
        return width;
    }


    public void setWidth(double width) {
        this.width = width;
        this.virtualWidth = width;
        setCoordinates();
    }

    public int getNumberOfSides(){
        return numberOfSides;
    }

    @Override
    public void addToToolBar(Controller controller) {
        controller.addPolygonToToolBar(this);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position[] getCoordinates() {
        return this.coordinates;

    }

    public double getReductionCoefficient() {
        return reductionCoefficient;
    }

    public void setReductionCoefficient(double reductionCoefficient) {
        this.reductionCoefficient = reductionCoefficient;
    }

    public double getVirtualWidth() {
        return virtualWidth;
    }

    public void setVirtualWidth(double virtualWidth) {
        this.virtualWidth = virtualWidth;
    }
}

