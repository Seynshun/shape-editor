package shapeImplementation;



public interface Editor {

    /**
     * Initializes the editor of a form
     * @param shape The shape we want the editor to take
     */
    public void initialisation(Shape shape);

    /**
     * Undoes changes made to a shape in the editor
     * @param controller The rendering engine for the editor
     */
    public void cancelModification(Controller controller);

    /**
     * Gets initial color of a shape before modification
     * @return The initial Color
     */
    public Color getInitialColor();

}
