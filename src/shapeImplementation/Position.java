package shapeImplementation;

import java.util.Objects;

/**
 * Class that represents the position of a shape
 */
public class Position {

    private double x ;
    private double y ;

    public Position(double x, double y){
        this.x= x;
        this.y =y;
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Double.compare(position.x, x) == 0 &&
                Double.compare(position.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString(){
        return String.format("Position(%s,%s)",this.x,this.y);
    }

    /**
     * Returns the distance between x and y coordinates
     * @param p The position containing the coordinates x and y
     * @return The distance
     */
    public double getDistance(Position p){
        return Math.sqrt( (Math.pow(x-p.getX(),2) ) + (Math.pow(y - p.getY(),2))  );
    }
}

