package shapeImplementation;

import factory.AbstractShapeFactory;
import factory.ShapeFactory;

/**
 * Facade pattern
 */
public class ShapeFacade {

  Shape rectangle;
  Shape polygon;
  ToolBar toolBar;
  Canvas canvas;
  Controller controller;

  public ToolBar getToolBar() {
    return toolBar;
  }

  public ShapeFacade(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    rectangle = shapeFactory.getShape("Rectangle");
    polygon = shapeFactory.getShape("Polygon");
    toolBar = new ToolBar();
    canvas = Canvas.getInstance();
  }

  public void setController(Controller controller) {
    this.controller = controller;
  }

  /**
   * Initializes the default toolbar containing a polygon and a rectangle
   */
  public void initializeToolbar(){
      toolBar.addShape(rectangle);
      toolBar.addShape(polygon);
      canvas.setToolBar(toolBar);
  }

  /**
   * Displays the toolbar on the rendering engine
   * @param controller The rendering engine
   */
  public void drawToolBar(Controller controller){
    toolBar.draw(controller);
  }


}
