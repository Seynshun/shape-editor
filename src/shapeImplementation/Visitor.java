package shapeImplementation;

/**
 * Visitor pattern
 */
public interface Visitor {
    
    public void visitPolygon(Polygon p);
    public void visitRectangle(Rectangle r);
    public void visitGroupShape(GroupShape gs);

}