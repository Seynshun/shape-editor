package shapeImplementation;

import java.util.ArrayList;

public interface Controller {

    public void initialisation();
    public void groupShape();
    public void unGroupShape();
    public void resizeModifier();
    public void drawRectangle(Rectangle shape);
    public void drawPolygon(Polygon shape);
    public void drawGroupShape(GroupShape shape);
    public void draw(Shape shape);
    public void drawShapeToDrawPane(Shape shape);
    public void addShapeToToolBar(Shape shape);
    public void addRectangleToToolBar(Rectangle s);
    public void addPolygonToToolBar(Polygon s);
    public void addGroupShapeToToolBar(GroupShape s);
    public void drawToolBar(ArrayList<Shape> shapes);
    public void removeShapeFromToolBar(Shape s);
    public void removeShapeFromDrawPane(Shape s);
    public void undoButton();
    public void redoButton();
    public void refreshWindow();
    public void save();
    public void load();
    public String toString();

}