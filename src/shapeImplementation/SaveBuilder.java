package shapeImplementation;

import java.io.File;
import java.util.ArrayList;

public interface SaveBuilder {

  /**
   * Initializes the serialization of a document by initializing the fields that will be contained in the file.
   */
  void startSave();

  /**
   * Saves the shapes contained in the drawing area to the file
   * @param drawingAreaShapes The shapes contained in the drawing area to be saved
   */
  void saveRightPaneShapes(ArrayList<Shape> drawingAreaShapes);

  /**
   * Saves the shapes contained in the toolbar to the file
   * @param toolBar The toolbar to be saved
   */
  void saveToolBar(ToolBar toolBar);

  /**
   * Finishes saving a file by adding in the file the fields initialized in startSave() method
   * @param file The backup file containing the data
   */
  void endSave(File file);

}
