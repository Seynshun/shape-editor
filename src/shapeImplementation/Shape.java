package shapeImplementation;


import history.Snapshot;
import java.util.ArrayList;
import java.util.Iterator;

public interface Shape extends Cloneable {

    /**
     * Clone a shape
     * @return the clone
     */
    public Shape clone();

    /**
     * Draw a shape on the renderer passed in parameter
     * @param controller Interface containing the methods of a rendering engine
     */
    public void draw(Controller controller);

    /**
     * Increments the coordinates of a shape by x and y
     * @param x
     * @param y
     */
    public void translate(double x, double y);

    /**
     * Changes the rotation of a shape
     * @param rotation
     */
    public void rotate(double rotation);

    /**
     * Verifies that the x and y coordinates belong to the shape
     * @param x
     * @param y
     * @return True if the coordinates belong to the shape
     */
    public boolean isInside(double x, double y);

    /**
     * Gets shape name
     * @return the name
     */
    public String getName();

    /**
     * Gets shape position
     * @return the position
     */
    public Position getPosition();

    /**
     * Set shape color
     * @param color the new color
     */
    public void setColor(Color color);

    /**
     * Gets shape color
     * @return the color
     */
    public Color getColor();

    /**
     * Gets shape angle
     * @return the angle
     */
    public double getAngle();

    /**
     * Gets shape size
     * @return the size
     */
    public double getSize();
    /**
     * Gets the coordinates of the corners
     * @return a table of coordinates
     */
    public Position[] getCornerCoordinates();

    /**
     *  Updates the coordinates of each point of a shape to move the shape to its new position
     */
    public void setCoordinates();

    /**
     * Returns the coordinates of a shape
     * @return a position composed of an x and y coordinate
     */
    public Position[] getCoordinates();

    //Iterator pattern

    /**
     * Creating an iterator for the iterator pattern
     * @return the iterator created
     */
    public Iterator<Shape> createIterator();

    /**
     * Browse recursively through the GroupShape aggregate to find all shapes available in GroupShape
     * @param result The resulting list of all shapes found
     */
    public void findAllShapes(ArrayList<Shape> result);


    //Factory pattern

    /**
     * Starts the initialization of the shape editor.
     */
    public void edit();
    
    //Composite pattern

    /**
     * Adds a shape to a Shape Group
     * @param shape the shape to be added
     */
    public void addShape(Shape shape);


    /**
     * Gets the list of shapes in a Shape Group
     * @return the list of shapes
     */
    public ArrayList<Shape> getShapes();

    /**
     * Changes the position of a shape group
     * @param pos the new position
     */
    public void setPosition(Position pos);

    /**
     * Restores the original size of a shape
     */
    public void resize();

    /**
     * Reduces the size of a shape
     * @param coefficient The coefficient of reduction
     */
    public void reduce(double coefficient);

    /**
     * Changes the size of a shape
     * @param coefficient The resize coefficient
     */
    public void setSize(double coefficient);

    /**
     * Adds a shape to the toolbar
     * @param controller The rendering engine to position the shape in the toolbar
     */
    public void addToToolBar(Controller controller);
    
    //Observer pattern
    public void addObserver(ShapeObserver ob);
    public void removeObserver(ShapeObserver ob);
    public void notifyObservers();
    
    //Visitor pattern
    public void accept(Visitor v);



}
