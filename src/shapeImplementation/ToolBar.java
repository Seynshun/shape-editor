package shapeImplementation;

import java.util.ArrayList;

/**
 * Class that represents the toolbar of shapes
 */
public class ToolBar {

  /**
   * list of shapes contained in the toolbar
   */
  private ArrayList<Shape> toolbar = new ArrayList<>();

  /**
   * Adds a list of shapes to the toolbar
   * @param shapes The list of shapes
   */
  public void addShape(ArrayList<Shape> shapes){
    for(Shape shape : shapes){
      toolbar.add(shape);
    }
  }

  /**
   * Adds a shape to the toolbar
   * @param shape The shape to add
   */
  public void addShape(Shape shape){
    if(toolbar.contains(shape)){
      return;
    }
    toolbar.add(shape);
  }

  /**
   * Removes a shape from the toolbar
   * @param shape The shape to be deleted
   */
  public void removeShape(Shape shape){
    toolbar.remove(shape);
  }

  /**
   * Removes all shapes from the toolbar
   */
  public void clearToolBar(){
    toolbar.clear();
  }

  /**
   * Draw the shapes contained in the toolbar
   * @param controller
   */
  public void draw(Controller controller){
    controller.drawToolBar(this.toolbar);
  }


  public ArrayList<Shape> getShapes(){
    return this.toolbar;
  }

  /**
   * Removes all shapes from the toolbar and adds a list of shapes to the toolbar.
   * @param shapes The list of shapes
   */
  public void restore(ArrayList<Shape> shapes){
    this.toolbar.clear();
    for(Shape s : shapes){
      this.toolbar.add(s);
    }
  }

  /**
   * Saves the current state of the toolbar
   * @return The list of shapes in the toolbar at the time of saving
   */
  public ArrayList<Shape> createBackup(){
    ArrayList<Shape> backup = new ArrayList<Shape>();
    for(Shape s: toolbar){
      backup.add(s.clone());
    }
    return backup;
  }

  public String toString(){
    String result = "ToolBar composée de : \n";
    for (Shape s : toolbar){
      result += s.toString() + "\n";

    }
    return result;
  }

}
