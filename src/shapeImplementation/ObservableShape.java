/**
 * Pattern observer
 */

package shapeImplementation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class ObservableShape implements Shape {
    private List<ShapeObserver> observersOrdered = new LinkedList<ShapeObserver>();
	private Set<ShapeObserver> observersSet = new HashSet<ShapeObserver>();

	protected String name;

	public ObservableShape(String name){
		this.name = name;
	}
	@Override
    public void addObserver(ShapeObserver obs){
        if (!observersSet.contains(obs)) {
			observersOrdered.add(obs);
			observersSet.add(obs);
		}
    }

    @Override
	public void removeObserver(ShapeObserver obs) {
		observersOrdered.remove(obs);
		observersSet.remove(obs);
	}

	@Override
	public void notifyObservers() {
		Object[] copy = observersOrdered.toArray();
		for (Object s: copy)
			((ShapeObserver) s).update(this);
	}
	
	public Shape clone(){
		Shape clone = null;

        try{
            clone =(ObservableShape) super.clone();
        }catch ( CloneNotSupportedException e){
            e.printStackTrace();
        }
        return clone;
	}

}