package shapeImplementation;

/**
 * Class that represents the color of a shape
 */
public class Color {

  /**
   * red color
   */
  private int r;
  /**
   * green color
   */
  private int g;
  /**
   * blue color
   */
  private int b;

  public Color(int r, int g, int b){
    this.r = r;
    this.g = g;
    this.b = b;
  }

  public int getR() {
    return r;
  }

  public void setR(int r) {
    this.r = r;
  }

  public int getG() {
    return g;
  }

  public void setG(int g) {
    this.g = g;
  }

  public int getB() {
    return b;
  }

  public void setB(int b) {
    this.b = b;
  }

  public String toString(){
    return String.format("Couleur(%s,%s,%s)",r,g,b);
  }
}
