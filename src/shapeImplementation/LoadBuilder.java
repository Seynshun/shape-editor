package shapeImplementation;

import java.io.File;

public interface LoadBuilder {

  /**
   * Initializes the reading of a file by retrieving the saved toolbar and the drawing area in the file.
   * @param file The file to be loaded
   * @param controller The rendering Engine
   * @return true if the file can be loaded
   */
  boolean startLoad(File file,Controller controller);

  /**
   * Read the data in the file corresponding to the shapes in the drawing area to restore them.
   */
  void loadRightPaneShapes();

  /**
   *  Read the data in the file corresponding to the shapes in toolbar to restore them.
   * @param toolBar
   */
  void loadToolBar(ToolBar toolBar);

}
