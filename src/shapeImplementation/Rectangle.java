package shapeImplementation;

import editors.RectangleEditor;
public class Rectangle extends BasedShape {

    private double width;
    private double height;
    private Editor editor;
    private double reductionCoefficient;
    private double virtualWidth, virtualHeight;

    public Rectangle(String name, Position position, Color color, double width, double height) {
        super(name, position, color,0);
        setWidth(width);
        setHeight(height);
        this.editor= RectangleEditor.getInstance();
        initCoordinates();
        reductionCoefficient = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(Controller rendering) {
        rendering.drawRectangle(this);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
        this.virtualWidth = width;
        setCoordinates();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
        this.virtualHeight = height;
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(Visitor v) {
        v.visitRectangle(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInside(double x, double y) {
        //https://math.stackexchange.com/questions/190111/how-to-check-if-a-point-is-inside-a-rectangle
        //Calculate the sum of all the triangles
        double somme = 0.0;
        Position pointPosition = new Position(x,y);
        setCoordinates();
        for(int i = 0; i<coordinates.length; i++){
            if(i == coordinates.length -1){
                
                double tmp = triangleArea(coordinates[i], coordinates[0],pointPosition);
                if (tmp == 0){
                    return true;
                }
                somme += tmp;
            }
            else{

                double tmp = triangleArea(coordinates[i], coordinates[i+1], pointPosition);
                somme += tmp;
            }

        }
        return ( Math.abs( somme - this.height * this.width) <1 );

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Shape clone() {
        Rectangle s = null;
        s = (Rectangle) super.clone();
        s.setReductionCoefficient(reductionCoefficient);
        s.setVirtualHeight(virtualHeight);
        s.setVirtualWidth(virtualWidth);
        s.rotate(angle);
        return s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void edit() {
        editor.initialisation(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rotate(double rotation){
        this.angle = rotation;
        
        this.setCoordinates();
    }

    private double triangleArea(Position p1, Position p2, Position p3){
        double a = p1.getDistance(p2);
        double b = p1.getDistance(p3);
        double c = p2.getDistance(p3);
        double p = (a+b+c)/2;
        double result = Math.sqrt( p * (p-a) * (p-b) * (p-c));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public void reduce(double coefficient){
        if(reductionCoefficient != 0){
            setCoordinates();
            return;
        }

        
        virtualHeight = height;
        virtualWidth = width;
        reductionCoefficient = coefficient;
        
        height = height * coefficient;
        width = width * coefficient;
    
        
        setCoordinates();
        

    }

    /**
     * {@inheritDoc}
     */
    public void resize(){

        height = virtualHeight;
        width = virtualWidth;
        
        setCoordinates();
        reductionCoefficient = 0;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public double getSize(){
        double y_min= coordinates[0].getY();
        double y_max =coordinates[0].getY() ;


        for(int i = 0; i < 4; i ++){
            double point = coordinates[i].getY();
            if(point <=  y_min ){
                y_min = point;
            }
            else if(point >= y_max){
                y_max = point;
            }
        }
        
        return Math.abs(y_max - y_min);
    }

    public void initCoordinates(){
        double x = position.getX();
        double y = position.getY();
        this.coordinates = new Position[4];
        coordinates[0] = new Position(x - width/2, y - height/2); //Top-left corner
        coordinates[1] = new Position(x +width/2, y - height/2); //Top-right corner
        coordinates[2] = new Position(x + width/2, y + height/2); //bottom-right corner
        coordinates[3] = new Position(x - width/2, y + height/2); //bottom-left corner
    }

    /**
     * {@inheritDoc}
     */
    
    @Override
    public void setCoordinates() {
        double cx = position.getX();
        double cy = position.getY();
        initCoordinates();

        for(int i = 0 ; i <4; i ++){
            Position corner = coordinates[i];
            double tempX = corner.getX() - cx;
            double tempY = corner.getY() - cy;
            //convert degree to radiant
            double radiantAngle = angle* (Math.PI/180);
            // now apply rotation
            double rotatedX = tempX*Math.cos(radiantAngle) - tempY*Math.sin(radiantAngle);
            double rotatedY = tempX*Math.sin(radiantAngle) + tempY*Math.cos(radiantAngle);
            corner.setX(rotatedX + cx);

            corner.setY(rotatedY + cy);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPosition(Position pos){
        this.position = pos;
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSize(double coefficient) {
        setHeight(height + (height *coefficient/100));
        setWidth(width + (width * coefficient/100));
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addToToolBar(Controller controller) {
        controller.addRectangleToToolBar(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position[] getCoordinates() {
        return this.coordinates;
    }

    public double getReductionCoefficient() {
        return reductionCoefficient;
    }

    public void setReductionCoefficient(double reductionCoefficient) {
        this.reductionCoefficient = reductionCoefficient;
    }

    public double getVirtualHeight() {
        return virtualHeight;
    }

    public double getVirtualWidth() {
        return virtualWidth;
    }

    public void setVirtualHeight(double virtualHeight) {
        this.virtualHeight = virtualHeight;
    }

    public void setVirtualWidth(double virtualWidth) {
        this.virtualWidth = virtualWidth;
    }
}
