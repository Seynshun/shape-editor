package shapeImplementation;


import editors.GroupEditor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import util.ShapeIterator;

/**
 * Class representing a group of shapes
 */
public class GroupShape extends ObservableShape  {

    private List<Shape> shapes;
    private Position position;
    private Position[] coordinates;
    private Position[] virtualCoordinates;
    private ArrayList<Position> virtualPositions = new ArrayList<Position>();


    private Editor editor;
    private Iterator iterator;
    private double angle;
    private double reductionCoefficient;

	public GroupShape(String name) {
        super(name);
        shapes = new ArrayList<Shape>();
        position = getPosition();
        this.editor = GroupEditor.getInstance();
        this.angle = 0;
        this.reductionCoefficient = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addShape(Shape shape){
        if(!shapes.contains(shape)){
            shapes.add(shape);
        }
        
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Shape clone() {
        GroupShape gs = new GroupShape(this.name);
        gs.setVirtualChildrenPositions(virtualPositions);
        
        for(Shape s: shapes){
            gs.addShape(s.clone());
        }
        return gs;
    }

    public void setVirtualChildrenPositions(ArrayList<Position> virtualPositions2) {
        virtualPositions.clear();
        for(Position pos : virtualPositions2){
            virtualPositions.add(new Position(pos.getX(),pos.getY()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void translate(double x, double y) {

        if(reductionCoefficient == 0){
            ArrayList<Shape> allShapes = new ArrayList<Shape>();
            findAllShapes(allShapes);

            for(Shape s : allShapes){
                s.translate(x, y);
            }
            setCoordinates();
        }
        if(reductionCoefficient != 0){
            setPosition(new Position(position.getX()+x,position.getY()+y));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rotate(double rotation) {
        ArrayList<Shape> allShapes = new ArrayList<Shape>();
        findAllShapes(allShapes);

        for(Shape s : allShapes){
            s.rotate(s.getAngle()+rotation);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInside(double x, double y) {
        /**
         * Récupération des x min et max et y min et max sur les figures 
         */
        double x_min = coordinates[0].getX();
        double x_max = coordinates[2].getX();
        double y_min = coordinates[0].getY();
        double y_max = coordinates[2].getY();
        
        return ( x> x_min && x < x_max && y>y_min && y< y_max);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position getPosition() {
        Position pos = new Position(0,0); 
        double lastX= 0;
        double lastY = 0;

        for(Shape s: shapes){
            double x= s.getPosition().getX();
            double y= s.getPosition().getY();
            lastX +=x;
            lastY +=y;
        }
        pos.setX(lastX/shapes.size());
        pos.setY(lastY/shapes.size());

        return pos;


    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void setColor(Color color) {
        for(Shape s: shapes){
            s.setColor(color);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Color getColor() {
        return shapes.get(0).getColor();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<Shape> getShapes(){
        return new ArrayList<Shape>(this.shapes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPosition(Position pos) {
        
        ArrayList<Shape> allShapes = new ArrayList<Shape>();
        findAllShapes(allShapes);
        if(reductionCoefficient ==0){
           //Resize
           double xTranslate = pos.getX()-virtualPositions.get(0).getX() ;
           double yTranslate = pos.getY()-virtualPositions.get(0).getY() ;


           for(int i = 0; i < shapes.size(); i ++){
               Position newPos = virtualPositions.get(i+1);
               
                double xTranslation = xTranslate+ newPos.getX();
                double yTranslation = yTranslate + newPos.getY();
               Shape s = shapes.get(i);
               s.setPosition(new Position(xTranslation, yTranslation));
           }
        }
        
        else{
            double xTranslation = pos.getX() - position.getX();
            double yTranslation = pos.getY() - position.getY();
            
            
            for(Shape s : allShapes){
                try {
                    //C'est un groupe
                    s.getShapes();
                    Position currentPos = s.getPosition();
                    Position newPos = new Position(currentPos.getX()+xTranslation, currentPos.getY()+ yTranslation);
                    s.setPosition(newPos);
                } catch (Exception e) {
                    s.translate(xTranslation,yTranslation);
                }
                
                
            }
        }

        this.position = pos;
        setCoordinates();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(Visitor v){
        v.visitGroupShape(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        String result =this.name + position.toString() +" composé de { \n %s";
        String temp = "";
        for (Shape s :shapes){
            temp += s.toString()+" ";
        }
        temp += "}";
        return String.format(result,temp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(Controller controller) {
        controller.drawGroupShape(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void edit() {
        editor.initialisation(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getAngle(){
        return this.angle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getSize(){
        //récupérer le plus haut x, et le plus au y

        double y_min = shapes.get(0).getPosition().getY();
        double y_max = shapes.get(0).getPosition().getY();

        for(Shape s: shapes){
            Position[] coordinates = s.getCoordinates();
            for(Position p : coordinates){
                if(y_min > p.getY()){
                    y_min = p.getY();
                }
                if(y_max< p.getY()){
                    y_max= p.getY();
                }
            }
        }
        

        return Math.abs(y_max - y_min);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position[] getCornerCoordinates() {
        return coordinates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCoordinates() {

        coordinates = new Position[4];
        double x_min = shapes.get(0).getCornerCoordinates()[0].getX();
        double x_max = shapes.get(0).getCornerCoordinates()[0].getX();
        double y_min = shapes.get(0).getCornerCoordinates()[0].getY();
        double y_max = shapes.get(0).getCornerCoordinates()[0].getY();

        ArrayList<Shape> allShapes = new ArrayList<Shape>();
        findAllShapes(allShapes);

        for(Shape s : allShapes){
            Position[] shapeCoordinates = s.getCornerCoordinates();
            for(int i = 0 ; i < shapeCoordinates.length; i++){
                Position corner = shapeCoordinates[i];
                if(x_min > corner.getX()){
                    x_min = corner.getX();
                }
                if(x_max < corner.getX()){
                    x_max =corner.getX();
                }
                if(y_min > corner.getY()){
                    y_min = corner.getY();
                }
                if(y_max < corner.getY()){
                    y_max = corner.getY();
                }

            }

        }

        coordinates[0]= new Position(x_min, y_min); //top-left corner
        coordinates[1]= new Position(x_max, y_min); //top-right corner
        coordinates[2]= new Position(x_max, y_max); //bottom-right corner
        coordinates[3]= new Position(x_min, y_max); //bottom-left corner

        this.position = getPosition();

      

    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setSize(double coefficient){
        ArrayList<Shape> allShapes = new ArrayList<Shape>();
        findAllShapes(allShapes);

        for(Shape s : allShapes){
            s.setSize(coefficient);
        }
        setCoordinates();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<Shape> createIterator() {
	      if (iterator == null){
	          iterator = new ShapeIterator(shapes.iterator());
        }
        return iterator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void findAllShapes(ArrayList<Shape> result){
        ShapeIterator iterator = new ShapeIterator(shapes.iterator());

        while (iterator.hasNext()){
            Shape shape = (Shape) iterator.next();
            shape.findAllShapes(result);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void reduce(double coeff){
        if(reductionCoefficient != 0){
            setCoordinates();
            return;
        }
        virtualCoordinates =coordinates;
        reductionCoefficient = coeff;
        
        
        for(int i = 0; i < coordinates.length; i++){
            Position lastPos = coordinates[i];
            coordinates[i]=new Position(lastPos.getX()*coeff,lastPos.getY()*coeff );
        }
        virtualPositions.add(this.position);
        for(int i = 0 ; i < shapes.size(); i++){
            Shape s = shapes.get(i);
            Position lastPos = s.getPosition();
			virtualPositions.add(new Position(lastPos.getX(), lastPos.getY()));
            s.reduce(coeff);
            s.setPosition(new Position(lastPos.getX() *coeff,lastPos.getY() *coeff));
        }
        setCoordinates();
        
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void resize(){
        if(reductionCoefficient ==0){
            setCoordinates();
            return;
        }
        coordinates = virtualCoordinates;
        for(int i = 0; i < shapes.size(); i++){
            Shape s = shapes.get(i);
            Position lastPos = virtualPositions.get(i+1);
            s.setPosition(new Position(lastPos.getX(),lastPos.getY()));
            s.resize();
        }
        setCoordinates();
        reductionCoefficient = 0;
        virtualPositions.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addToToolBar(Controller controller) {
        controller.addGroupShapeToToolBar(this);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position[] getCoordinates() {
        return this.coordinates;

    }

    public ArrayList<Position> getVirtualPositions(){
        return this.virtualPositions;
    }





}
