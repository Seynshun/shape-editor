package shapeImplementation;

import builder.JsonLoadBuilder;
import builder.JsonSaveBuilder;
import commands.BackupCommand;
import history.Snapshot;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing all the components of the canvas
 */
public class Canvas {

    /**
     * Shapes that are in the drawing area
     */
    public ArrayList<Shape> drawingAreaShapes = new ArrayList<Shape>();
    /**
     * Shapes that have been selected in the drawing area  to form a group
     */
    public ArrayList<Shape> selectedShapes = new ArrayList<>();

    private static Canvas mainCanvas;

    public ToolBar toolBar;

    /**
     * History of states between each executed command
     */
    public List<History> history = new ArrayList<History>();
    /**
     * Current state of history
     */
    private int virtualSize = 0;

    public static Canvas getInstance() {
        if (mainCanvas == null) {
            mainCanvas = new Canvas();
        }
        return mainCanvas;
    }

    public int getVirtualSize() {
        return virtualSize;
    }

    public void setVirtualSize(int virtualSize) {
        this.virtualSize = virtualSize;
    }

    public void setToolBar(ToolBar toolBar) {
        this.toolBar = toolBar;
    }

    /**
     * Create a backup of the current state of the canvas
     * @param command The command that caused the backup
     */
    public void createBackup(Command command) {

        ArrayList<Shape> newRightPaneShapes = new ArrayList<Shape>();
        ArrayList<Shape> newToolBarShapes = new ArrayList<Shape>();

        for (Shape s : drawingAreaShapes) {
            newRightPaneShapes.add(s.clone());
        }

        newToolBarShapes = toolBar.createBackup();
        
        System.out.println("create backup " + virtualSize);
        if(virtualSize != history.size() && virtualSize > 0){
            history = history.subList(0, virtualSize);
        }
        Snapshot backup = new Snapshot(newRightPaneShapes, newToolBarShapes);
        history.add(new History(command, backup));
        this.virtualSize = history.size();

    }

    public void addShapeToSelectedShape(Shape shape) {
        if (selectedShapes.contains(shape)) {
            return;
        }
        selectedShapes.add(shape);
    }

    public void addShapeToDrawingArea(Shape shape) {
        if (drawingAreaShapes.contains(shape)) {
            return ;
        }
        drawingAreaShapes.add(shape);
    }

    public void addShapeToToolBar(Shape shape) {

        toolBar.addShape(shape);
    }

    /**
     * Removes all shapes from the drawingArea and adds a new list of shapes to the drawingArea.
     * @param shapes The list of shapes
     */
    public void restore(ArrayList<Shape> shapes) {
        
        this.drawingAreaShapes.clear();
        for(Shape s : shapes){
            drawingAreaShapes.add(s);
        }
    }

    /**
     * Restores the canvas to its previous state
     */
    public void undo() {
        int virtualSize = mainCanvas.getVirtualSize();
        if (virtualSize <= 1) {
            System.out.println("nothing to restore--VirtualSize = " + virtualSize);
        } else {
            virtualSize = Math.max(0, virtualSize - 1);
            System.out.println("Restore Backup " + (virtualSize - 1));

            Snapshot backup = mainCanvas.history.get(virtualSize - 1).getSnapshot();
            mainCanvas.restore(backup.getDrawingPaneMap());
            toolBar.restore(backup.getToolBarMap());
            mainCanvas.setVirtualSize(virtualSize);
        }
    }

    /**
     * Restores the state before undo of the canvas
     */
    public void redo() {
        int virtualSize = mainCanvas.getVirtualSize();
        if (virtualSize == mainCanvas.history.size()) {
            System.out.println("nothing to redo --VirtualSize = " + virtualSize);
            return;
        } else {
            virtualSize = Math.min(mainCanvas.history.size(), virtualSize + 1);
            System.out.println("Redo Backup " + (virtualSize));

            Snapshot backup = mainCanvas.history.get(virtualSize - 1).getSnapshot();
            mainCanvas.restore(backup.getDrawingPaneMap());
            toolBar.restore(backup.getToolBarMap());
            mainCanvas.setVirtualSize(virtualSize);
        }

    }

    /**
     * Serializes the canvas into a document
     * @param file The file in which we save
     */
    public void saveFile(File file) {

        SaveBuilder saveBuilder = new JsonSaveBuilder();
        saveBuilder.startSave();
        saveBuilder.saveRightPaneShapes(drawingAreaShapes);
        saveBuilder.saveToolBar(toolBar);
        saveBuilder.endSave(file);
    }

    /**
     * Only serializes the toolbar in a document
     * @param file The file in which we save
     */
    public void saveToolBar(File file){
        SaveBuilder saveBuilder = new JsonSaveBuilder();
        saveBuilder.startSave();
        saveBuilder.saveToolBar(toolBar);
        saveBuilder.endSave(file);
    }

    /**
     * Loads a serialized document into the canvas
     * @param controller The rendering engine
     * @param file The file to load
     * @return true if the file can be loaded
     */
    public boolean loadFile(Controller controller, File file) {
        
        drawingAreaShapes.clear();
        toolBar.clearToolBar();
        BackupCommand initCommand = new BackupCommand();
        initCommand.execute();
        LoadBuilder loadBuilder = new JsonLoadBuilder();
        if(!loadBuilder.startLoad(file,controller)){
            return false;
        }
        loadBuilder.loadRightPaneShapes();
        loadBuilder.loadToolBar(toolBar);
        controller.refreshWindow();
        history.clear();
        virtualSize = 0;
        return true;
    }


	public void addGroupShapeToRightPane(GroupShape groupShape) {
        if (drawingAreaShapes.contains(groupShape)) {
            return ; 
        }
        drawingAreaShapes.add(0,groupShape);
	}

}