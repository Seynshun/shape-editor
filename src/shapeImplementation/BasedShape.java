package shapeImplementation;


import history.Snapshot;
import java.util.ArrayList;
import java.util.Iterator;
import util.NullIterator;


public abstract class BasedShape extends ObservableShape {

    protected Position position;
    private Color color;
    protected double angle;
    protected Position[] coordinates;

    public BasedShape(String name, Position position,Color color,double angle){

        super(name);
        this.position = position;
        this.color = color;
        this.angle = angle;
    }
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Shape clone() {
        Shape clone = null;
        clone =(BasedShape) super.clone();
        clone.setColor(new Color(this.getColor().getR(),this.getColor().getG(),this.getColor().getB()));
        clone.setPosition(new Position(this.getPosition().getX(),this.getPosition().getY()));
        return clone;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void translate(double x, double y) {
        position.setX(position.getX() + x);
        position.setY(position.getY() + y);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void rotate(double rotation) {

    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName(){
        return this.name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position getPosition() { return this.position; }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addShape(Shape shape){
        throw new UnsupportedOperationException();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<Shape> getShapes(){
        throw new UnsupportedOperationException();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void accept(Visitor v);
    /**
     * {@inheritDoc}
     */
    @Override
    public void setPosition(Position pos){
        this.position = pos;
        notifyObservers();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Color getColor() {
        return color;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setColor(Color color) {
        this.color = color;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        return String.format("%s -> %s; %s ; Size= %s; @%s", this.name,color.toString(), this.position.toString(),getSize(),Integer.toHexString(this.hashCode()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getAngle(){
        return this.angle;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Position[] getCornerCoordinates(){
        setCoordinates();
        return this.coordinates;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<Shape> createIterator() {
        return new NullIterator();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void findAllShapes(ArrayList<Shape> result) {
        result.add(this);
    }

}
