package javafx;

import commands.BackupCommand;
import commands.RedoCommand;
import commands.UndoCommand;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.visitors.JavafxRepresentationVisitor;
import javafx.visitors.JavafxDrawShapeVisitor;
import javafx.visitors.JavafxDrawToolBarVisitor;
import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;
import util.INode;

public class JavaFxController implements Controller {

    MouseListener mouseListener;

    double windowSize;
    ToolBar toolBar;

    private Canvas mainCanvas = Canvas.getInstance();
    public INode<Shape, javafx.scene.shape.Shape> rightPaneTree = new INode<Shape, javafx.scene.shape.Shape>(
            new GroupShape("root"), null);
    public INode<Shape, javafx.scene.shape.Shape> toolBarTree = new INode<Shape, javafx.scene.shape.Shape>(
            new GroupShape("root"), null);

    public Stage editorStage = null;
    private Stage primaryStage;
    public Stage popupStage = new Stage();
    private int maxShapesInToolBar = 7;
    File toolBarState = new File("src/files/toolbarState.json");

    @FXML
    public MenuBar menuBar;
    public VBox window;
    public Pane editBar;
    public AnchorPane leftAnchor;
    public SplitPane splitPane;
    public Pane rightPane;
    public Line trash;
    public StackPane stackPane;

    @FXML
    public void initialisation() {

        mainCanvas.loadFile(this, toolBarState);
        double trashPosition = leftAnchor.getHeight() - stackPane.getHeight() ;
        mouseListener.setTrashPosition(trashPosition);
        mouseListener.openEditor(rightPane);
        mouseListener.drag(rightPane);
        mouseListener.dragFromToolBar(leftAnchor, toolBar);
    }

    public Pane getRightPane() {
        return this.rightPane;
    }

    public void groupShape() {
        if (mainCanvas.selectedShapes.size() > 1) {
            GroupShape groupShape = new GroupShape("group");

            INode<Shape, javafx.scene.shape.Shape> newGroup = new INode<Shape, javafx.scene.shape.Shape>(groupShape,
                    null);

            for (int i = 0; i < mainCanvas.selectedShapes.size(); i++) {
                Shape shape = mainCanvas.selectedShapes.get(i);
                INode<Shape, javafx.scene.shape.Shape> nodeShape = rightPaneTree.getChild(shape);
                rightPaneTree.getChildren().remove(rightPaneTree.getChild(shape));
                mainCanvas.drawingAreaShapes.remove(shape);
                newGroup.addChild(nodeShape);
                groupShape.addShape(shape);

            }
            rightPaneTree.getKey().addShape(groupShape);
            rightPaneTree.addChild(newGroup);
            mainCanvas.addGroupShapeToRightPane(groupShape);

            mainCanvas.selectedShapes.clear();
            BackupCommand backup = new BackupCommand();
            backup.execute();

        }
    }

    @Override
    public void unGroupShape() {
        if (mainCanvas.selectedShapes.size() != 0) {

            for (int i = 0; i < mainCanvas.selectedShapes.size(); i++) {
                
                Shape shape = mainCanvas.selectedShapes.get(i);
                mainCanvas.drawingAreaShapes.remove(shape);
                ArrayList<Shape> result = new ArrayList<>();
                shape.findAllShapes(result);
                if (result.size() >1) {
                    INode<Shape, javafx.scene.shape.Shape> nodeShape = rightPaneTree
                        .getChild(shape);
                    rightPaneTree.getChildren().remove(nodeShape);
                    nodeShape.getChildren().remove(shape);


                    for (INode node : nodeShape.getChildren()) {
                        mainCanvas.drawingAreaShapes.add((Shape) node.getKey());
                        rightPaneTree.getChildren().add(node);
                    }
                }
            }
            mainCanvas.selectedShapes.clear();
        }
    }

    public void resizeModifier() {
        VBox.setVgrow(splitPane, Priority.ALWAYS); // fill Vbox contents
        leftAnchor.maxWidthProperty().bind(splitPane.widthProperty().multiply(0.10)); // limits the resizing of the
                                                                                      // toolbar
    }

    public void drawRectangle(Rectangle rectangle) {

        rectangle.resize();
        javafx.scene.shape.Polygon javafxRectangle = getRectangleRepresentation(rectangle);
        rightPane.getChildren().add(javafxRectangle);

        for (INode<Shape, javafx.scene.shape.Shape> node : rightPaneTree.getLeaves()) {
            if (node.getKey() == rectangle) {
                node.setValue(javafxRectangle);
                return;
            }
            
        }
        mainCanvas.addShapeToDrawingArea(rectangle);

        rightPaneTree.addChild(new INode<Shape, javafx.scene.shape.Shape>(rectangle, javafxRectangle));

    }

    public void drawPolygon(Polygon polygon) {
        polygon.resize();
        javafx.scene.shape.Polygon javafxPolygon = getPolygonRepresentation(polygon);

        INode<Shape, javafx.scene.shape.Shape> newNode;
        rightPane.getChildren().add(javafxPolygon);

        for (INode<Shape, javafx.scene.shape.Shape> node : rightPaneTree.getLeaves()) {
            if (node.getKey() == polygon) {

                node.setValue(javafxPolygon);
                return;
            }
        }
        mainCanvas.addShapeToDrawingArea(polygon);

        newNode = new INode<Shape, javafx.scene.shape.Shape>(polygon, javafxPolygon);
        rightPaneTree.addChild(newNode);
    }


    public void drawToolBar(ArrayList<Shape> shapes) {

        for (Shape s : shapes) {
            drawToolBar(s);
        }
    }

    public void drawToolBar(Shape s) {

        JavafxDrawToolBarVisitor toolbarPosition = new JavafxDrawToolBarVisitor(this);
        s.accept(toolbarPosition);

    }

    @Override
    public void addShapeToToolBar(Shape s) {
        s.addToToolBar(this);
        mainCanvas.saveToolBar(toolBarState);
    }

    @Override
    public void addRectangleToToolBar(Rectangle s) {
        if(rectangleToolBarPosition(s)){    
            mainCanvas.addShapeToToolBar(s);
        }
    }

    @Override
    public void addPolygonToToolBar(Polygon s) {
        if(polygonToolBarPosition(s)){

            mainCanvas.addShapeToToolBar(s);
        }
    }

    @Override
    public void addGroupShapeToToolBar(GroupShape s) {
        if(groupShapeToolBarPosition(s)){

            mainCanvas.addShapeToToolBar(s);
        }
    }

    public boolean groupShapeToolBarPosition(GroupShape s) {

        INode<Shape, javafx.scene.shape.Shape> javafxShapes = new INode<Shape, javafx.scene.shape.Shape> (null,null);
        JavafxRepresentationVisitor representationVisitor = new JavafxRepresentationVisitor(javafxShapes);

        double sizeOfAPlace = (leftAnchor.getHeight() - stackPane.getHeight()) / maxShapesInToolBar;

        double newY = getNextPlaceHolder();
        if (newY == 0) {
            s.translate(40, 0);
            drawGroupShape(s);
            System.out.println("please delete shapes");
            
            return false ;
        }
        double coefficient = sizeOfAPlace / s.getSize();

        s.reduce(coefficient);
        s.setPosition(new Position(leftAnchor.getWidth() / 2, newY + s.getSize() / 2));
        
        s.setCoordinates();

        INode<Shape, javafx.scene.shape.Shape> groupShapeNode = new INode<Shape, javafx.scene.shape.Shape>(s, null);
        
        for (Shape shape : s.getShapes()) {
            shape.accept(representationVisitor);
            
        }
        
        javafxShapes = representationVisitor.getRepresentation();
        List< INode<Shape, javafx.scene.shape.Shape>> leaves = javafxShapes.getLeaves();
        
        for(int i = 0 ; i <leaves.size(); i ++){
            INode<Shape, javafx.scene.shape.Shape> leaf = leaves.get(i);
            javafx.scene.shape.Shape javafxShape = leaf.getValue();
            
            leftAnchor.getChildren().add(javafxShape);
        }
        groupShapeNode.addChildren(javafxShapes.getChildren());
        toolBarTree.addChild(groupShapeNode);
        return true;

    }

    private double getNextPlaceHolder() {
        
        double result = 10;
        double space = 10;

        for (INode<Shape, javafx.scene.shape.Shape> nodeShape : toolBarTree.getChildren()) {
            
            result += nodeShape.getKey().getSize() + space;
        }
        if (result >= leftAnchor.getHeight() - (stackPane.getHeight() * 110) / 100) {
            return 0;
        }
        return result;
    }

    public boolean rectangleToolBarPosition(Shape s) {


        double sizeOfAPlace = (leftAnchor.getHeight() - stackPane.getHeight()) / maxShapesInToolBar;
        Rectangle rectangle = (Rectangle) s;
        double newY = getNextPlaceHolder();
        if (newY == 0) {
            Position pos = new Position(rectangle.getWidth(),rectangle.getPosition().getY() );
            s.setPosition(pos);
            drawRectangle(rectangle);
            System.out.println("please delete shapes");
            return false;
        }
        double coefficient = sizeOfAPlace / rectangle.getSize();
        rectangle.reduce(coefficient);
        rectangle.setPosition(new Position(leftAnchor.getWidth() / 2, newY + rectangle.getSize() / 2));
        rectangle.setCoordinates();
        javafx.scene.shape.Polygon javafxRectangle = getRectangleRepresentation(s);
        leftAnchor.getChildren().add(javafxRectangle);
        toolBarTree.addChild(new INode<Shape, javafx.scene.shape.Shape>(s, javafxRectangle));
        return true;
    }

    public boolean polygonToolBarPosition(Shape s) {
        double sizeOfAPlace = (leftAnchor.getHeight() - stackPane.getHeight()) / maxShapesInToolBar;
        Polygon polygon = (Polygon) s;
        double newY = getNextPlaceHolder();
        if (newY == 0) {
            Position pos = new Position(polygon.getWidth(),polygon.getPosition().getY() );
            s.setPosition(pos);
            drawPolygon(polygon);
            System.out.println("please delete shapes");
            return false;
        }

        double coefficient = sizeOfAPlace / polygon.getSize();
        polygon.reduce(coefficient);
        polygon.setPosition(new Position(leftAnchor.getWidth() / 2, newY + polygon.getSize() / 2));
        polygon.setCoordinates();

        javafx.scene.shape.Polygon javafxPolygon = getPolygonRepresentation(s);

        leftAnchor.getChildren().add(javafxPolygon);
        toolBarTree.addChild(new INode<Shape, javafx.scene.shape.Shape>(s, javafxPolygon));
        return true;
    }

    private javafx.scene.shape.Polygon getRectangleRepresentation(Shape shape) {
        Rectangle rectangle = (Rectangle) shape.clone();
        rectangle.setPosition(new Position(shape.getPosition().getX(), shape.getPosition().getY()));

        javafx.scene.shape.Polygon javafxRectangle = new javafx.scene.shape.Polygon();

        Position[] coordinates = rectangle.getCornerCoordinates();
        Double[] allPoints = new Double[8];
        for (int i = 0, j = 0; i < coordinates.length; i++) {
            Position corner = coordinates[i];
            allPoints[j] = corner.getX();
            j++;
            allPoints[j] = corner.getY();
            j++;
        }
        javafxRectangle.getPoints().addAll(allPoints);
        javafxRectangle.setFill(
                Color.rgb(rectangle.getColor().getR(), rectangle.getColor().getG(), rectangle.getColor().getB(), 1));
        javafxRectangle.setRotate(shape.getAngle());

        return javafxRectangle;
    }

    private javafx.scene.shape.Polygon getPolygonRepresentation(Shape shape) {
        Polygon polygon = (Polygon) shape.clone();

        polygon.setPosition(new Position(shape.getPosition().getX(), shape.getPosition().getY()));

        Position[] coordinates = polygon.getCornerCoordinates();
        Double[] allPoints = new Double[polygon.getNumberOfSides() * 2];
        for (int i = 0, j = 0; i < coordinates.length; i++) {
            Position corner = coordinates[i];
            allPoints[j] = corner.getX();
            j++;
            allPoints[j] = corner.getY();
            j++;
        }
        javafx.scene.shape.Polygon javafxPolygon = new javafx.scene.shape.Polygon();
        javafxPolygon.getPoints().addAll(allPoints);

        javafxPolygon
                .setFill(Color.rgb(polygon.getColor().getR(), polygon.getColor().getG(), polygon.getColor().getB(), 1));
        return javafxPolygon;
    }

    @Override
    public void refreshWindow() {
        this.rightPaneTree = new INode<Shape, javafx.scene.shape.Shape>(new GroupShape("root"),
                null);
        this.toolBarTree = new INode<Shape, javafx.scene.shape.Shape>(new GroupShape("root"),
                null);

        for (Iterator<Node> it = rightPane.getChildren().iterator(); it.hasNext();) {
            Node node = it.next();
            if (node instanceof javafx.scene.shape.Shape) {
                it.remove();
            }
        }

        for (Iterator<Node> it = leftAnchor.getChildren().iterator(); it.hasNext();) {
            Node node = it.next();
            if (node instanceof javafx.scene.shape.Shape) {
                it.remove();
            }
        }
        for( Shape s: mainCanvas.drawingAreaShapes){
            System.out.println(s.toString());

            System.out.println(mainCanvas.drawingAreaShapes.size());
            s.draw(this);
            System.out.println(mainCanvas.drawingAreaShapes.size());
        }
        for(Shape s: toolBar.getShapes()){
            drawToolBar(s);
        }

    }

    @Override
    public void undoButton() {
        Command undoCommand = new UndoCommand();
        undoCommand.execute();
        refreshWindow();
    }

    @Override  
    public void redoButton() {

        Command redoCommand = new RedoCommand();
        redoCommand.execute();
        refreshWindow();
    }

    @Override
    public void save() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Fichier source JSON (.json)",
                "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);
        if (file != null) {
            mainCanvas.saveFile(file);
        }

    }

    @Override
    public void load() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open file");
        File file = fileChooser.showOpenDialog(primaryStage);
        boolean cantLoad = false;
        if (file != null)
            cantLoad = mainCanvas.loadFile(this, file);

        if (cantLoad == false) {
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Warning");
            Label label1 = new Label("You didn't choose a valid file");
            Button button1 = new Button("Close");
            button1.setOnAction(e -> popupStage.close());
            VBox layout = new VBox(10);
            layout.getChildren().addAll(label1, button1);
            layout.setAlignment(Pos.CENTER);
            Scene scene1 = new Scene(layout, 300, 250);
            popupStage.setScene(scene1);
            popupStage.showAndWait();
        }

    }

    @Override
    public void draw(Shape shape) {
        JavafxDrawShapeVisitor representationVisitor = new JavafxDrawShapeVisitor(this, rightPaneTree);
        shape.accept(representationVisitor);
    }

    @Override
    public void drawShapeToDrawPane(Shape s) {
        if (s.getName().equalsIgnoreCase("Rectangle")) {
            Rectangle rectangle = (Rectangle) s;
            javafx.scene.shape.Polygon javafxRectangle = getRectangleRepresentation(rectangle);
            leftAnchor.getChildren().add(javafxRectangle);
            mainCanvas.addShapeToDrawingArea(s);
            rightPaneTree.addChild(new INode<Shape, javafx.scene.shape.Shape>(rectangle, javafxRectangle));
        }

        if (s.getName().equalsIgnoreCase("Polygon")) {
            javafx.scene.shape.Polygon javafxPolygon = getPolygonRepresentation(s);
            leftAnchor.getChildren().add(javafxPolygon);
            mainCanvas.addShapeToDrawingArea(s);
            rightPaneTree.addChild(new INode<Shape, javafx.scene.shape.Shape>(s, javafxPolygon));
        }

    }

    public String toString() {
        String result = "Controller composé de : \n";
        result += "Drawing Pane composé de \n";
        result += rightPaneTree.toString() + "\n";
        for(INode<Shape, javafx.scene.shape.Shape> child : rightPaneTree.getChildren()){
            result += child.toString() + "\n";
        }
        /*
         * result += "toolBar composé de \n"; for (INode<Shape,
         * javafx.scene.shape.Shape> s : toolBarTree.getChildren()) { result +=
         * s.getKey().toString() + " \n"; }
         */
        return result;
    }

    public String toStringV2() {
        String result = "Controller composé de : \n";
        for (Iterator<Node> it = rightPane.getChildren().iterator(); it.hasNext();) {
            Node node = it.next();
            if (node instanceof javafx.scene.shape.Shape) {
                result +=node+ " "+ Integer.toHexString(node.hashCode()) + "\n";
            }
        }
        
        return result;
    }

    public void setMouseListener(MouseListener mouseListener) {
        this.mouseListener = mouseListener;
        this.mouseListener.setController(this);
    }

    public void setWindow() {
        double trashPosition = leftAnchor.getHeight() - stackPane.getHeight() ;
        mouseListener.setTrashPosition(trashPosition);
    }

    public void setWindowMaximize(double windowSize) {
        this.windowSize = windowSize;
        double trashPosition = windowSize- menuBar.getPrefHeight() - stackPane.getPrefHeight() - editBar.getPrefHeight();
        mouseListener.setTrashPosition(trashPosition);
    }

    public void setToolBar(ToolBar toolBar) {
        this.toolBar = toolBar;
    }

    @Override
    public void removeShapeFromToolBar(Shape s) {
        toolBarTree.deleteChild(s);
        refreshToolBar();
        mainCanvas.saveToolBar(toolBarState);

    }

    @Override
    public void removeShapeFromDrawPane(Shape s) {
        INode<Shape, javafx.scene.shape.Shape> node = rightPaneTree.getChild(s);
        if(node.getValue()==null){
            //C'est un groupe
            List<INode<Shape, javafx.scene.shape.Shape>> leaves =node.getLeaves();
            for(INode<Shape, javafx.scene.shape.Shape> leaf : leaves){
                rightPane.getChildren().remove(leaf.getValue());
                mainCanvas.drawingAreaShapes.remove(leaf.getKey());
            }
        }
        else{
            rightPane.getChildren().remove(node.getValue());
            mainCanvas.drawingAreaShapes.remove(node.getKey());
        }

        rightPaneTree.deleteChild(s);

    }

    public void setStage(Stage stage) {
        this.primaryStage = stage;
    }

    public void refreshToolBar() {
        toolBarTree = new INode<Shape, javafx.scene.shape.Shape>(new GroupShape("root"),
                null);

        for (Iterator<Node> it = leftAnchor.getChildren().iterator(); it.hasNext();) {
            Node node = it.next();
            if (node instanceof javafx.scene.shape.Shape) {
                it.remove();
            }
        }
        drawToolBar(mainCanvas.toolBar.getShapes());

    }

    @Override
    public void drawGroupShape(GroupShape shape) {
        shape.resize();
        INode<Shape, javafx.scene.shape.Shape> newNode;
        if (rightPaneTree.getDirectChild(shape) == null) {

            // Si c'est un nouveau group
            if (rightPaneTree.getChild(shape) == null) {
                newNode = new INode<Shape, javafx.scene.shape.Shape>(shape, null);
                rightPaneTree.addChild(newNode);

                mainCanvas.addShapeToDrawingArea(shape);
                for (Shape s : shape.getShapes()) {
                    INode<Shape, javafx.scene.shape.Shape> newChild = new INode<Shape, javafx.scene.shape.Shape>(s,
                            null);
                    newNode.addChild(newChild);
                    s.draw(this);
                }
                rightPaneTree.getKey().addShape(shape);
            }

            // Si c'est un sous-groupe
            else {
                newNode = rightPaneTree.getChild(shape);
                newNode.getChildren().clear();
                for (Shape s : shape.getShapes()) {
                    INode<Shape, javafx.scene.shape.Shape> newChild = new INode<Shape, javafx.scene.shape.Shape>(s,
                            null);
                    newNode.addChild(newChild);
                    s.draw(this);
                }

            }
        }

        // si c'est un group
        else {
            newNode = rightPaneTree.getChild(shape);
            mainCanvas.addShapeToDrawingArea(shape);
            newNode.getChildren().clear();

            for (Shape s : shape.getShapes()) {
                INode<Shape, javafx.scene.shape.Shape> newChild = new INode<Shape, javafx.scene.shape.Shape>(s, null);
                newNode.addChild(newChild);
                s.draw(this);
            }
        }

    }
}
