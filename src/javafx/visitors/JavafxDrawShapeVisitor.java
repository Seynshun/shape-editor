package javafx.visitors;

import javafx.JavaFxController;
import javafx.scene.shape.Shape;
import shapeImplementation.Canvas;
import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Rectangle;
import shapeImplementation.Visitor;
import util.INode;

public class JavafxDrawShapeVisitor implements Visitor {

    public Shape shape;
    public shapeImplementation.Shape state;
    private JavaFxController controller;
    private Canvas mainCanvas = Canvas.getInstance();
    private INode<shapeImplementation.Shape, javafx.scene.shape.Shape> parent;


    public JavafxDrawShapeVisitor(JavaFxController controller, INode<shapeImplementation.Shape, javafx.scene.shape.Shape> parent){
        this.controller = controller;
        this.parent = parent;
    }
    @Override
    public void visitPolygon(Polygon polygon) {

        JavafxRepresentationVisitor representationVisitor = new JavafxRepresentationVisitor(null);
        polygon.resize();
        polygon.accept(representationVisitor);
        javafx.scene.shape.Shape javafxPolygon = representationVisitor.getRepresentation().getLeaves().get(0).getValue();
        
        INode<shapeImplementation.Shape, javafx.scene.shape.Shape> newNode;
        controller.rightPane.getChildren().add(javafxPolygon);
        
        for(INode<shapeImplementation.Shape, javafx.scene.shape.Shape> node : controller.rightPaneTree.getLeaves()){
            if(node.getKey()==polygon){
                
                node.setValue(javafxPolygon);
                return;
            }
        }
        mainCanvas.addShapeToDrawingArea(polygon);

        newNode = new INode<shapeImplementation.Shape, javafx.scene.shape.Shape>(polygon,javafxPolygon);
        parent.addChild(newNode);

    }

    @Override
    public void visitRectangle(Rectangle rectangle) {

        JavafxRepresentationVisitor representationVisitor = new JavafxRepresentationVisitor(null);

        rectangle.resize();

        rectangle.accept(representationVisitor);
        javafx.scene.shape.Shape javafxRectangle = representationVisitor.getRepresentation().getLeaves().get(0).getValue();
        controller.rightPane.getChildren().add(javafxRectangle);
        
        for(INode<shapeImplementation.Shape, javafx.scene.shape.Shape> node : controller.rightPaneTree.getLeaves()){
            if(node.getKey()==rectangle){
                node.setValue(javafxRectangle);
                return;
            }
        }
        
        mainCanvas.addShapeToDrawingArea(rectangle);
        parent.addChild(new INode<shapeImplementation.Shape, javafx.scene.shape.Shape>(rectangle,javafxRectangle));

    }

    @Override
    public void visitGroupShape(GroupShape gs){
        controller.drawGroupShape(gs);
    }

    public Shape getRepresentation(){
        return this.shape;
    }
    
}