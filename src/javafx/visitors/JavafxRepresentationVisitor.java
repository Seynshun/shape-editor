package javafx.visitors;


import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.Visitor;
import util.INode;

public class JavafxRepresentationVisitor implements Visitor {

    public shapeImplementation.Shape state;
    public INode<shapeImplementation.Shape, Shape> parent;


    public JavafxRepresentationVisitor(INode<shapeImplementation.Shape, Shape> parent){
        this.parent = parent;
    }


    @Override
    public void visitPolygon(Polygon p) {
        Polygon polygon = (Polygon) p.clone();

        polygon.setPosition(new Position(p.getPosition().getX(), p.getPosition().getY()));

        Position[] coordinates = polygon.getCornerCoordinates();
        Double[] allPoints = new Double[polygon.getNumberOfSides() * 2];
        for (int i = 0, j = 0; i < coordinates.length; i++) {
            Position corner = coordinates[i];
            allPoints[j] = corner.getX();
            j++;
            allPoints[j] = corner.getY();
            j++;
        }
        javafx.scene.shape.Polygon javafxPolygon = new javafx.scene.shape.Polygon();
        javafxPolygon.getPoints().addAll(allPoints);

        javafxPolygon
                .setFill(Color.rgb(polygon.getColor().getR(), polygon.getColor().getG(), polygon.getColor().getB(), 1));
        
        INode<shapeImplementation.Shape, Shape> leaf = new INode<shapeImplementation.Shape, Shape>(p,javafxPolygon);
        parent.addChild(leaf);
    
    }

    @Override
    public void visitRectangle(Rectangle r) {
        Rectangle rectangle = (Rectangle) r.clone();
        rectangle.setPosition(new Position(r.getPosition().getX(), r.getPosition().getY()));

        javafx.scene.shape.Polygon javafxRectangle = new javafx.scene.shape.Polygon();

        Position[] coordinates = rectangle.getCornerCoordinates();
        Double[] allPoints = new Double[8];
        for (int i = 0, j = 0; i < coordinates.length; i++) {
            Position corner = coordinates[i];
            allPoints[j] = corner.getX();
            j++;
            allPoints[j] = corner.getY();
            j++;
        }
        javafxRectangle.getPoints().addAll(allPoints);
        javafxRectangle.setFill(
                Color.rgb(rectangle.getColor().getR(), rectangle.getColor().getG(), rectangle.getColor().getB(), 1));
        javafxRectangle.setRotate(r.getAngle());

        INode<shapeImplementation.Shape, Shape> leaf = new INode<shapeImplementation.Shape, Shape>(r,javafxRectangle);
        parent.addChild(leaf);
        
    }

    @Override
    public void visitGroupShape(GroupShape gs){
        
        INode<shapeImplementation.Shape, Shape> node = new INode<shapeImplementation.Shape, Shape>(gs,null);
        INode<shapeImplementation.Shape, Shape> tmpParent = parent;

        parent.addChild(node);
        
        for(shapeImplementation.Shape s: gs.getShapes()){
            parent = node;
            s.accept(this);
        }

        parent = tmpParent;

    }

    public INode<shapeImplementation.Shape, Shape> getRepresentation(){
        
        return this.parent;
    }
    
}