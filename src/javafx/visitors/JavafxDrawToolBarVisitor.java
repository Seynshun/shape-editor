package javafx.visitors;

import javafx.JavaFxController;
import javafx.scene.shape.Shape;
import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Rectangle;
import shapeImplementation.Visitor;
import util.INode;

public class JavafxDrawToolBarVisitor implements Visitor {

    public INode<Shape,shapeImplementation.Shape> state;
    public JavaFxController controller;

    public JavafxDrawToolBarVisitor(JavaFxController controller){
        this.controller = controller;
    }
    @Override
    public void visitPolygon(Polygon p) {
        controller.polygonToolBarPosition(p);
    }

    @Override
    public void visitRectangle(Rectangle r) {
        controller.rectangleToolBarPosition(r);
    }

    @Override
    public void visitGroupShape(GroupShape gs) {
        controller.groupShapeToolBarPosition(gs);
        
    }

    public INode<Shape,shapeImplementation.Shape> getNode(){
        return state;
    }
    
}