package javafx;

import commands.BackupCommand;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import shapeImplementation.ShapeFacade;

public class JavaFXInitialisation extends Application {

  public void launchApplication(){
    launch();
  }

  @Override
  public void start(Stage stage) throws Exception {

    ShapeFacade facade = new ShapeFacade();

    FXMLLoader fxml = new FXMLLoader(getClass().getResource("../editor.fxml"));
    Parent root = fxml.load();
    Scene scene = new Scene(root);

    JavaFxController controller = fxml.getController();
    facade.setController(controller);
    facade.initializeToolbar();

    stage.setTitle("Shape Editor");
    stage.setScene(scene);
    stage.show();

    facade.drawToolBar(controller);

    controller.resizeModifier();
    controller.setMouseListener(new MouseListener());
    controller.setStage(stage);
    controller.setWindow();
    controller.setToolBar(facade.getToolBar());

    controller.initialisation();

    stage.maximizedProperty().addListener(new ChangeListener<Boolean>() {

      @Override
      public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
        stage.heightProperty().addListener((obs, oldVal, newVal) -> {
          controller.setWindowMaximize(stage.getHeight());
        });

      }
    });

    stage.heightProperty().addListener((obs, oldVal, newVal) -> {
      controller.setWindow();
    });


    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
      @Override
      public void handle(WindowEvent windowEvent) {
        Platform.exit();
      }
    });


    BackupCommand command = new BackupCommand();
    command.execute();
  }
}
