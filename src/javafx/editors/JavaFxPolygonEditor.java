package javafx.editors;

import commands.BackupCommand;
import editors.PolygonEditor;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import javafx.JavaFxController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import shapeImplementation.Canvas;
import shapeImplementation.Controller;
import shapeImplementation.Polygon;

public class JavaFxPolygonEditor  {

    final ColorPicker colorPicker = new ColorPicker();
    final TextField newNumberOfSides = new TextField();
    final TextField newLength = new TextField();
    final TextField newAngle = new TextField();

    final Color[] color = new Color[1];
    JavaFxController controller;
    PolygonEditor polygonEditor = PolygonEditor.getInstance();

    Canvas mainCanvas = Canvas.getInstance();


    public void render(Controller controller) {
        this.controller = (JavaFxController) controller;

        Polygon shape = polygonEditor.getShape();
        this.controller.editorStage = new Stage();

        color[0] = new Color((double) shape.getColor().getR() / 255, (double) shape.getColor().getG() / 255,
                (double) shape.getColor().getB() / 255, 1.0);
        colorPicker.setValue(color[0]);

        colorPicker.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                color[0] = colorPicker.getValue();
            }
        });

        GridPane root = new GridPane();
        Button okButton = new Button("Ok");
        Button applyButton = new Button("Appliquer");
        Button cancelButton = new Button("Annuler");
        Label label1 = new Label("Number of sides:");
        Label label2 = new Label("Lenght:");
        Label label3 = new Label("Angle:");

        newAngle.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newAngle.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newAngle.setText(Double.toString(shape.getAngle()));
                }
            }
        });

        newAngle.setText(Double.toString(shape.getAngle()));

        newNumberOfSides.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newNumberOfSides.getText().matches("[1-9]+")) {
                    newNumberOfSides.setText(Integer.toString(shape.getNumberOfSides()));
                }
            }
        });

        newNumberOfSides.setText(Integer.toString(shape.getNumberOfSides()));

        newLength.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newLength.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newLength.setText(Double.toString(shape.getWidth()));
                }
            }
        });
        newLength.setText(Double.toString(shape.getWidth()));

        GridPane.setConstraints(colorPicker, 0, 0);
        GridPane.setConstraints(label1, 0, 1);
        GridPane.setConstraints(newNumberOfSides, 1, 1);
        GridPane.setConstraints(label2, 0, 2);
        GridPane.setConstraints(newLength, 1, 2);
        GridPane.setConstraints(label3, 0, 3);
        GridPane.setConstraints(newAngle, 1, 3);

        GridPane.setConstraints(okButton, 0, 4);
        GridPane.setConstraints(applyButton, 1, 4);
        GridPane.setConstraints(cancelButton, 2, 4);

        Pane pane = (this.controller).rightPane;

        applyButton(applyButton, color, shape, pane);
        okButton(okButton, color, shape, this.controller.editorStage, pane);
        cancelButton(cancelButton, shape, pane);

        root.setHgap(10);
        root.getChildren().addAll(colorPicker, applyButton, okButton, cancelButton, label1, label2, label3, newNumberOfSides,
            newLength, newAngle);

        Scene scene = new Scene(root);
        PointerInfo a = MouseInfo.getPointerInfo();
        Point mouseCoordinate = a.getLocation();
        double x = mouseCoordinate.getX();
        double y = mouseCoordinate.getY();

        this.controller.editorStage.setX(x);
        this.controller.editorStage.setY(y);
        this.controller.editorStage.toFront();
        this.controller.editorStage.setResizable(false);
        this.controller.editorStage.initStyle(StageStyle.TRANSPARENT);
        this.controller.editorStage.setAlwaysOnTop(true);
        this.controller.editorStage.setScene(scene);
        this.controller.editorStage.show();
    }

    public void applyButton(Button applyButton, Color color[], Polygon s, Pane pane) {
        applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                pane.getChildren().remove(controller.rightPaneTree.getChild(s).getValue());
                polygonEditor.setCanCancel(true);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));

                double newWidthValue = Double.parseDouble(newLength.getText());
                int newNumberOfSidesValue = Integer.parseInt(newNumberOfSides.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());
                polygonEditor.applyModification(controller,newColor,newWidthValue,newNumberOfSidesValue,newAngleValue);

            }
        });
    }

    public void okButton(Button okButton, Color color[], Polygon s, Stage stage, Pane pane) {
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                pane.getChildren().remove(controller.rightPaneTree.getChild(s).getValue());
                polygonEditor.setCanCancel(false);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));

                double newWidthValue = Double.parseDouble(newLength.getText());
                int newNumberOfSidesValue = Integer.parseInt(newNumberOfSides.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());
                polygonEditor.applyModification(controller,newColor,newWidthValue,newNumberOfSidesValue,newAngleValue);

                stage.close();
                mainCanvas.createBackup(new BackupCommand());
            }
        });
    }

    public void cancelButton(Button cancelButton, Polygon shape, Pane pane) {
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (polygonEditor.isCanCancel() == true) {
                    pane.getChildren().remove(controller.rightPaneTree.getChild(shape).getValue());
                    polygonEditor.cancelModification(controller);
                    shapeImplementation.Color initialColor = polygonEditor.getInitialColor();
                    double initialAngle = polygonEditor.getInitialAngle();
                    double initialLength = polygonEditor.getInitialLength();
                    int initialNumberOfSides = polygonEditor.getInitialNumberOfSides();


                    final Color[] colorSetter = new Color[1];
                    colorSetter[0] = new Color((double) initialColor.getR() / 255, (double) initialColor.getG() / 255,
                            (double) initialColor.getB() / 255, 1.0);

                    newAngle.setText(Double.toString(initialAngle));
                    newLength.setText(Double.toString(initialLength));
                    newNumberOfSides.setText(Integer.toString(initialNumberOfSides));
                    colorPicker.setValue(colorSetter[0]);
                    color[0] = colorSetter[0];
                }
            }
        });
    }

}