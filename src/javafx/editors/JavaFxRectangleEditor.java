package javafx.editors;

import commands.BackupCommand;
import editors.RectangleEditor;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import javafx.JavaFxController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import shapeImplementation.Canvas;
import shapeImplementation.Controller;
import shapeImplementation.Rectangle;



public class JavaFxRectangleEditor {
    final ColorPicker colorPicker = new ColorPicker();
    final TextField newHeight = new TextField();
    final TextField newWidth = new TextField();
    final TextField newAngle = new TextField();

    final Color[] color = new Color[1];
    JavaFxController controller;
    Canvas mainCanvas = Canvas.getInstance();
    RectangleEditor rectangleEditor = RectangleEditor.getInstance();

    public void render(Controller controller) {
        this.controller = (JavaFxController) controller;

        Rectangle shape = rectangleEditor.getShape();

        this.controller.editorStage = new Stage();

        color[0] = new Color((double) shape.getColor().getR() / 255, (double) shape.getColor().getG() / 255,
                (double)shape.getColor().getB() / 255, 1.0);
        colorPicker.setValue(color[0]);

        colorPicker.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                color[0] = colorPicker.getValue();
            }
        });

        GridPane root = new GridPane();
        Button okButton = new Button("Ok");
        Button applyButton = new Button("Apply");
        Button cancelButton = new Button("Cancel");
        Label label1 = new Label("Height:");
        Label label2 = new Label("Width:");
        Label label3 = new Label("Angle:");
        
        newAngle.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) { // when focus lost
                if (!newAngle.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newAngle.setText(Double.toString(shape.getAngle()));
                }
            }
        });
        
        newAngle.setText(Double.toString(shape.getAngle()));

        newHeight.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) { // when focus lost
                if (!newHeight.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newHeight.setText(Double.toString(shape.getHeight()));
                }
            }
        });

        newHeight.setText(Double.toString(shape.getHeight()));

        newWidth.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newWidth.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newWidth.setText(Double.toString(shape.getWidth()));
                }
            }
        });
        newWidth.setText(Double.toString(shape.getWidth()));

        GridPane.setConstraints(colorPicker, 0, 0);
        GridPane.setConstraints(label1, 0, 1);
        GridPane.setConstraints(newHeight, 1, 1);
        GridPane.setConstraints(label2, 0, 2);
        GridPane.setConstraints(newWidth, 1, 2);
        GridPane.setConstraints(label3, 0, 3);
        GridPane.setConstraints(newAngle, 1, 3);

        GridPane.setConstraints(okButton, 0, 4);
        GridPane.setConstraints(applyButton, 1, 4);
        GridPane.setConstraints(cancelButton, 2, 4);


        Pane pane = (this.controller).rightPane;

        applyButton(applyButton, color, shape, pane);
        okButton(okButton, color, shape, this.controller.editorStage, pane);
        cancelButton(cancelButton, shape, pane);

        root.setHgap(10);
        root.getChildren().addAll(colorPicker, applyButton, okButton, cancelButton, label1, label2, label3, newAngle, newHeight,
                newWidth);

        Scene scene = new Scene(root);
        PointerInfo a = MouseInfo.getPointerInfo();
        Point mouseCoordinate = a.getLocation();
        double x = mouseCoordinate.getX();
        double y = mouseCoordinate.getY();

        this.controller.editorStage.setX(x);
        this.controller.editorStage.setY(y);
        this.controller.editorStage.toFront();
        this.controller.editorStage.setResizable(false);
        this.controller.editorStage.initStyle(StageStyle.TRANSPARENT);
        this.controller.editorStage.setAlwaysOnTop(true);
        this.controller.editorStage.setScene(scene);
        this.controller.editorStage.show();
    }

    public void applyButton(Button applyButton, Color color[], Rectangle s, Pane pane) {
        applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                pane.getChildren().remove(controller.rightPaneTree.getChild(s).getValue());
                rectangleEditor.setCanCancel(true);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));

                double newWidthValue = Double.parseDouble(newWidth.getText());
                double newHeightValue = Double.parseDouble(newHeight.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());
                rectangleEditor.applyModification(controller,newColor,newWidthValue,newHeightValue,newAngleValue);
            }
        });
    }

    public void okButton(Button okButton, Color color[], Rectangle s, Stage stage, Pane pane) {
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                pane.getChildren().remove(controller.rightPaneTree.getChild(s).getValue());

                rectangleEditor.setCanCancel(false);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));

                double newWidthValue = Double.parseDouble(newWidth.getText());
                double newHeightValue = Double.parseDouble(newHeight.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());

                rectangleEditor.applyModification(controller,newColor,newWidthValue,newHeightValue,newAngleValue);

                stage.close();
                mainCanvas.createBackup(new BackupCommand());
            }
        });
    }

    public void cancelButton(Button cancelButton, Rectangle shape, Pane pane) {
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (rectangleEditor.isCanCancel() == true) {
                    pane.getChildren().remove(controller.rightPaneTree.getChild(shape).getValue());
                    rectangleEditor.cancelModification(controller);

                    shapeImplementation.Color initialColor = rectangleEditor.getInitialColor();
                    double initialHeight = rectangleEditor.getInitialHeight();
                    double initialWidth = rectangleEditor.getInitialWidth();
                    double initialAngle = rectangleEditor.getInitialAngle();

                    final Color[] colorSetter = new Color[1];
                    colorSetter[0] = new Color((double) initialColor.getR() / 255, (double) initialColor.getG() / 255,
                            (double) initialColor.getB() / 255, 1.0);

                    newHeight.setText(Double.toString(initialHeight));
                    newWidth.setText(Double.toString(initialWidth));
                    newAngle.setText(Double.toString(initialAngle));

                    colorPicker.setValue(colorSetter[0]);
                    color[0] = colorSetter[0];
                }
            }
        });
    }
}
