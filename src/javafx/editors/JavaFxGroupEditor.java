package javafx.editors;

import commands.BackupCommand;
import editors.GroupEditor;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.util.ArrayList;
import javafx.JavaFxController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import shapeImplementation.Canvas;
import shapeImplementation.Controller;
import shapeImplementation.GroupShape;
import shapeImplementation.Position;
import shapeImplementation.Shape;
import util.INode;

public class JavaFxGroupEditor {

    final ColorPicker colorPicker = new ColorPicker();
    final TextField newSize = new TextField();
    final TextField newAngle = new TextField();

    final Color[] color = new Color[1];
    JavaFxController controller;

    GroupEditor groupEditor = GroupEditor.getInstance();

    boolean canCancel = true;
    double initialAngle;
    Position[] initialCoordinates;
    shapeImplementation.Color initialColor;
    ArrayList<shapeImplementation.Color> initialChildrenColors;
    ArrayList<Double> initialChildrenAngles;


    GroupShape shape;

    Canvas mainCanvas = Canvas.getInstance();
    GridPane root = new GridPane();
    Button okButton = new Button("Ok");
    Button applyButton = new Button("Appliquer");
    Button cancelButton = new Button("Annuler");
    Label label2 = new Label("Size :");
    Label label3 = new Label("Angle:");
    Label checkInfoLabel = new Label("Check to change all colors");
    CheckBox cbColor = new CheckBox("");



    public void render(Controller controller) {
        this.controller = (JavaFxController) controller;

        GroupShape shape = groupEditor.getShape();


        initialColor = shape.getColor();
        initialCoordinates = shape.getCornerCoordinates();
        initialAngle = shape.getAngle();
        initialChildrenColors = new ArrayList<shapeImplementation.Color>();
        initialChildrenAngles = new ArrayList<Double>();

        this.controller.editorStage = new Stage();
        for(Shape child: shape.getShapes()){
            initialChildrenColors.add(child.getColor());
            initialChildrenAngles.add(child.getAngle());
        }
        cbColor.setSelected(false);

        color[0] = new Color((double) shape.getColor().getR() / 255, (double) shape.getColor().getG() / 255,
                (double) shape.getColor().getB() / 255, 1.0);
        colorPicker.setValue(color[0]);

        colorPicker.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                color[0] = colorPicker.getValue();
            }
        });

        

        newAngle.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newAngle.getText().matches("([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newAngle.setText(Double.toString(shape.getAngle()));
                }
            }
        });

        newAngle.setText(Double.toString(shape.getAngle()));

        newSize.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (!newSize.getText().matches("([+-])?([0-9]{1,3}[.])?[0-9]{1,3}")) {
                    newSize.setText(Double.toString(0.0));
                }
            }
        });
        newSize.setText(Double.toString(0.0));

        GridPane.setConstraints(checkInfoLabel, 1, 0);

        GridPane.setConstraints(colorPicker, 0, 1);
        GridPane.setConstraints(cbColor, 1, 1);


        GridPane.setConstraints(label2, 0, 2);
        GridPane.setConstraints(newSize, 1, 2);

        GridPane.setConstraints(label3, 0, 3);
        GridPane.setConstraints(newAngle, 1, 3);

        GridPane.setConstraints(okButton, 0, 4);
        GridPane.setConstraints(applyButton, 1, 4);
        GridPane.setConstraints(cancelButton, 2, 4);

        Pane pane = (this.controller).rightPane;

        applyButton(applyButton, color, shape, pane);
        okButton(okButton, color, shape, this.controller.editorStage, pane);
        cancelButton(cancelButton, shape, pane);

        root.setHgap(10);
        root.getChildren().addAll(colorPicker, applyButton, okButton, cancelButton, label2, label3,
                newSize, newAngle,cbColor,checkInfoLabel);

        Scene scene = new Scene(root);
        PointerInfo a = MouseInfo.getPointerInfo();
        Point mouseCoordinate = a.getLocation();
        double x = mouseCoordinate.getX();
        double y = mouseCoordinate.getY();

        this.controller.editorStage.setX(x);
        this.controller.editorStage.setY(y);
        this.controller.editorStage.toFront();
        this.controller.editorStage.setResizable(false);
        this.controller.editorStage.initStyle(StageStyle.TRANSPARENT);
        this.controller.editorStage.setAlwaysOnTop(true);
        this.controller.editorStage.setScene(scene);
        this.controller.editorStage.show();
    }

    public void applyButton(Button applyButton, Color color[], GroupShape s, Pane pane) {
        applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getChild(s).getLeaves())
                    pane.getChildren().remove(nodeShape.getValue());
                groupEditor.setCanCancel(true);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));
                
                


                double newSizeValue = Double.parseDouble(newSize.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());
                if(!cbColor.isSelected()) newColor = null;
                groupEditor.applyModification(controller,newColor,newSizeValue,newAngleValue);


            }
        });
    }

    public void okButton(Button okButton, Color color[], GroupShape s, Stage stage, Pane pane) {
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getChild(s).getLeaves())
                    pane.getChildren().remove(nodeShape.getValue());

                groupEditor.setCanCancel(false);
                shapeImplementation.Color newColor = new shapeImplementation.Color((int) (color[0].getRed() * 255),
                        (int) (color[0].getGreen() * 255), (int) (color[0].getBlue() * 255));

                double newSizeValue = Double.parseDouble(newSize.getText());
                double newAngleValue = Double.parseDouble(newAngle.getText());
                if(!cbColor.isSelected()){
                    newColor=null;
                }
                groupEditor.validateModification(controller,newColor,newAngleValue);


                stage.close();
                mainCanvas.createBackup(new BackupCommand());
            }
        });
    }

    public void cancelButton(Button cancelButton, GroupShape shape, Pane pane) {
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (groupEditor.isCanCancel() == true) {
                    for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getChild(shape).getLeaves())
                        pane.getChildren().remove(nodeShape.getValue());
                    int i = 0;
                    for(Shape s: shape.getShapes()){
                        s.setColor(initialChildrenColors.get(i));
                        i++;
                    }
                    groupEditor.cancelModification(controller);
                    shapeImplementation.Color initialColor = groupEditor.getInitialColor();
                    double initialAngle = groupEditor.getInitialAngle();
                    double initialSize = groupEditor.getInitialSize();

                    final Color[] colorSetter = new Color[1];
                    colorSetter[0] = new Color((double) initialColor.getR() / 255, (double) initialColor.getG() / 255,
                            (double) initialColor.getB() / 255, 1.0);

                    newAngle.setText(Double.toString(0));
                    newSize.setText(Double.toString(0));
                    colorPicker.setValue(colorSetter[0]);
                    color[0] = colorSetter[0];
                }
            }
        });
    }

}