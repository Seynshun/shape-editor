package javafx;

import commands.BackupCommand;
import commands.DeleteShapeFromDrawPaneCommand;
import commands.DeleteShapeFromToolBarCommand;
import commands.MoveCommand;
import commands.MoveFromToolBarCommand;
import javafx.editors.JavaFxGroupEditor;
import javafx.editors.JavaFxPolygonEditor;
import javafx.editors.JavaFxRectangleEditor;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import shapeImplementation.Canvas;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;
import util.INode;

public class MouseListener {

  final ColorPicker colorPicker = new ColorPicker();
  final int LEFT_MARGIN = -20;
  final Color[] color = new Color[1];
  public double trashPosition;
  boolean release = false;
  private boolean canDrag = false;
  private Canvas mainCanvas = Canvas.getInstance();
  private JavaFxController controller;

  public void setTrashPosition(double trashPosition) {
    this.trashPosition = trashPosition;
  }

  public void setController(JavaFxController controller) {
    this.controller = controller;
  }

  public void selectToGroup(Pane pane, MouseEvent e) {

    for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getChildren()){
      Shape s = nodeShape.getKey();
      if (s.isInside(e.getX(), e.getY())) {
          mainCanvas.addShapeToSelectedShape(s);
        
      }
    }
  }

  public void dragFromToolBar(Pane from, ToolBar toolBar) {

    from.setOnDragDetected(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
          for (Shape s : mainCanvas.toolBar.getShapes()) {
            double mouseX = mouseEvent.getX();
            double mouseY = mouseEvent.getY();
            if (s.isInside(mouseX, mouseY)) {
              canDrag = true;
              releaseFromToolbar(from, s, toolBar);
            }
          }
        }
      }
    });

  }

  public void releaseFromToolbar(Pane from, Shape shape, ToolBar toolBar) {
    from.setOnMouseReleased(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
          double mouseX = mouseEvent.getX();
          double mouseY = mouseEvent.getY();

          if (canDrag) {

            if (mouseX > from.getWidth()) {
              Shape clone = shape.clone();
              MoveFromToolBarCommand moveCommand = new MoveFromToolBarCommand(clone, mouseX - from.getWidth(), mouseY,
                  controller);
              moveCommand.execute();
              BackupCommand backupCommand = new BackupCommand();
              backupCommand.execute();

            } else {
              if (mouseY > trashPosition) {
                from.getChildren().remove(controller.toolBarTree.getDirectChild(shape).getValue());

                DeleteShapeFromToolBarCommand deleteShapeCommand = new DeleteShapeFromToolBarCommand(toolBar, shape, controller);
                deleteShapeCommand.execute();
                BackupCommand backupCommand = new BackupCommand();
                backupCommand.execute();

              }
            }
          }
          canDrag = false;
        }
      }
    });
  }

  public void drag(Pane pane) {

    pane.setOnDragDetected(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {

        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
          
          double mouseX = mouseEvent.getX();
          double mouseY = mouseEvent.getY();

          for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getChildren()){
            
            Shape s = nodeShape.getKey();
            if (s.isInside(mouseX, mouseY)) {
              System.out.println(s.toString());
              release = true;
              release(pane, s);
              
            }

          }
          
        }
      }
    });

  }

  public void release(Pane pane, Shape shape) {
    pane.setOnMouseDragged(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        if (release == true) {
          double mouseX = mouseEvent.getX();
          double mouseY = mouseEvent.getY();
          
          if(controller.rightPaneTree.getDirectChild(shape).getValue() ==null){//c'est un groupe
            
            for(INode<Shape, javafx.scene.shape.Shape> nodeShape : controller.rightPaneTree.getDirectChild(shape).getLeaves())
              if(!pane.getChildren().remove(nodeShape.getValue()))
                System.out.println("Pas trouvé: " +controller.toStringV2());
          
          } 
          else
            pane.getChildren().remove(controller.rightPaneTree.getDirectChild(shape).getValue());
        
          if (mouseX < LEFT_MARGIN) {
            if(mouseY>trashPosition){
              DeleteShapeFromDrawPaneCommand deleteShapeCommand = new DeleteShapeFromDrawPaneCommand(shape, controller);
                deleteShapeCommand.execute();
                
            }
            else{

              controller.addShapeToToolBar(shape);
              mainCanvas.drawingAreaShapes.remove(shape);
              BackupCommand command = new BackupCommand();
              command.execute();  
            }
            release = false;
            
          } else {
            
            MoveCommand moveCommand = new MoveCommand(shape, mouseX - shape.getPosition().getX(),
                mouseY - shape.getPosition().getY(), controller);
            moveCommand.execute();
          }
        }
      }
    });
    pane.setOnMouseReleased(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent mouseEvent) {
        if(release==true){
          BackupCommand command = new BackupCommand();
          command.execute();
        }
        release = false;
      }
    });
  }

  public void openEditor(Pane pane) {

    pane.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent mouseEvent) {


        if (mouseEvent.isControlDown()) {
          System.out.println("Select to group");
          selectToGroup(pane, mouseEvent);

        }

        if(mouseEvent.isAltDown()){
          System.out.println("Test");
          System.out.println( controller.toString());

        }

        if (mouseEvent.getButton() == MouseButton.SECONDARY) {
          if (controller.editorStage != null) {
            controller.editorStage.close();
          }
          double mouseX = mouseEvent.getX();
          double mouseY = mouseEvent.getY();

          for (Shape s : mainCanvas.drawingAreaShapes) {

            if (s.isInside(mouseX, mouseY)) {

              s.edit();
              if (s.getName().equalsIgnoreCase("group")){
                JavaFxGroupEditor groupEditor = new JavaFxGroupEditor();
                groupEditor.render(controller);
              }

              else if (s.getName().equalsIgnoreCase("rectangle")){
                JavaFxRectangleEditor rectangleEditor = new JavaFxRectangleEditor();
                rectangleEditor.render(controller);
              }  
              else if(s.getName().equalsIgnoreCase("polygon")){
                JavaFxPolygonEditor polygonEditor = new JavaFxPolygonEditor();
                polygonEditor.render(controller);
              }  



            }
          }
          
        }
      }
    });
  }

}
