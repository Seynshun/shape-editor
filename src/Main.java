import javafx.JavaFXInitialisation;

public class Main{


    public static void main(String[] args) {

        JavaFXInitialisation javaFXInitialisation = new JavaFXInitialisation();
        javaFXInitialisation.launchApplication();
    }

}
