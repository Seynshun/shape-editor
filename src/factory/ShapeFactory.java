package factory;

import shapeImplementation.Color;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;

public class ShapeFactory implements AbstractShapeFactory {


  @Override
  public Shape getShape(String shape) {
    switch (shape.toUpperCase()){

      case "POLYGON":
        return new Polygon("Polygon", new Position(60,15),new Color(0,255,0),5,50);

      case "RECTANGLE":
        return new Rectangle("Rectangle", new Position(35,65),new Color(255,0,0),50,80);
    }
    return null;
  }
  

}
