package factory;

import shapeImplementation.Shape;

public interface AbstractShapeFactory {

    Shape getShape(String shape);


}
