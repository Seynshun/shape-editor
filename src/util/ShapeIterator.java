package util;

import java.util.Iterator;
import java.util.Stack;
import shapeImplementation.Shape;

public class ShapeIterator implements Iterator{

  final Stack<Iterator<Shape>> stack = new Stack<>();

  public ShapeIterator(Iterator iterator){
    stack.push(iterator);
  }

  @Override
  public boolean hasNext() {
    if (stack.isEmpty()) {
      return false;
    }
    else{
      Iterator<Shape> iterator = stack.peek();
      if (!iterator.hasNext()){
        stack.pop();
        return hasNext();
      }
      else{
        return true;
      }
    }
  }

  @Override
  public Object next() {
    if (hasNext()){
      Iterator<Shape> iterator = stack.peek();
      Shape shape = iterator.next();
      stack.push(shape.createIterator());
      return shape;
    }
    else{
      return null;
    }
  }
}
