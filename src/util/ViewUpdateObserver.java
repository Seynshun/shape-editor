package util;

import shapeImplementation.Shape;
import shapeImplementation.ShapeObserver;

public class ViewUpdateObserver implements ShapeObserver {


    @Override
    public void update(Shape shape) {
        //mise à jour de la vue
    }

}