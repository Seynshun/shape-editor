package util;

import java.util.ArrayList;
import java.util.List;

public class INode<T1, T2> {

    private List<INode<T1, T2>> children = new ArrayList<>();
    private T1 key = null;
    private T2 value = null;
    private INode<T1, T2> parent = null;

    public INode(T1 key, T2 value) {
        this.key = key;
        this.value = value;
    }

    public INode<T1, T2> addChild(INode<T1, T2> child) {
        if (!children.contains(child)) {
            child.setParent(this);
            this.children.add(child);
        }
        return child;
    }

    public void addChildren(List<INode<T1, T2>> children) {
        children.forEach(each -> each.setParent(this));
        this.children.addAll(children);
    }

    public List<INode<T1, T2>> getChildren() {
        return children;
    }

    public INode<T1, T2> getDirectChild(T1 key) {

        for (INode<T1, T2> child : children) {
            if (child.getKey() == key) {
                return child;
            }
        }

        return null;
    }

    public void deleteChild(T1 key) {
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).getKey() == key) {
                children.get(i).setParent(null);
                children.remove(i);
                return;
            }
        }
    }

    public INode<T1, T2> getChild(T1 k) {

        if (key != k) {
            for (INode<T1, T2> child : children) {
                INode<T1, T2> newNode = child.getChild(k);
                if (newNode != null) {
                    return newNode;
                }
            }
            return null;
        }

        return this;
    }

    public T1 getKey() {
        return key;
    }

    public void setKey(T1 key) {
        this.key = key;
    }

    public T2 getValue() {
        return value;
    }

    public void setValue(T2 value) {
        this.value = value;
    }

    private void setParent(INode<T1, T2> parent) {
        this.parent = parent;
    }

    public INode<T1, T2> getParent() {
        return parent;
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    public void deleteNode() {
        if (parent != null) {
            int index = this.parent.getChildren().indexOf(this);
            this.parent.getChildren().remove(this);
            for (INode<T1, T2> each : getChildren()) {
                each.setParent(this.parent);
            }
            this.parent.getChildren().addAll(index, this.getChildren());
        } else {
            deleteRootNode();
        }
        this.getChildren().clear();
    }

    public INode<T1, T2> deleteRootNode() {
        if (parent != null) {
            throw new IllegalStateException("deleteRootNode not called on root");
        }
        INode<T1, T2> newParent = null;
        if (!getChildren().isEmpty()) {
            newParent = getChildren().get(0);
            newParent.setParent(null);
            getChildren().remove(0);
            for (INode<T1, T2> each : getChildren()) {
                each.setParent(newParent);
            }
            newParent.getChildren().addAll(getChildren());
        }
        this.getChildren().clear();
        return newParent;
    }

    public List<INode<T1, T2>> getLeaves() {
        List<INode<T1, T2>> leaves = new ArrayList<INode<T1, T2>>();
        if (isLeaf()) {
            leaves.add(this);
            return leaves;
        } else {
            for (INode<T1, T2> node : children) {
                leaves.addAll(node.getLeaves());
            }
        }
        return leaves;

    }

    public List<T1> getKeyLeaves() {
        List<T1> leaves = new ArrayList<T1>();
        if (isLeaf()) {
            leaves.add(this.key);
            return leaves;
        } else {
            for (INode<T1, T2> node : children) {
                leaves.addAll(node.getKeyLeaves());
            }
        }
        return leaves;

    }

    public String toString() {
        String result = "";
        if (isLeaf()) {
            return String.format("\n->%s [%s]", this.key.toString(), this.value);
        }
        result += " \n Group{  ";
        for (INode<T1, T2> node : children) {
            result += node.toString();
        }
        result += " }";
        return result;
    }

}