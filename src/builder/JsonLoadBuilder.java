package builder;


import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import shapeImplementation.Color;
import shapeImplementation.Controller;
import shapeImplementation.GroupShape;
import shapeImplementation.LoadBuilder;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.ToolBar;

/**
 * Class for loading a backup file
 */
public class JsonLoadBuilder implements LoadBuilder {

  JSONArray rightPane;
  JSONArray toolBar;
  Controller controller;

  /**
   * Reads a file line by line
   * @param filePath The file to read
   * @return The content of the file in a string
   */
  private static String readLineByLineJava8(String filePath) {
    //https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
    StringBuilder contentBuilder = new StringBuilder();
    try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
      stream.forEach(s -> contentBuilder.append(s).append("\n"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return contentBuilder.toString();
  }

  /**
   * Verifies that the json is valid by containing the expected fields.
   * @param file The file to be checked
   * @return The file if it is valid, null otherwise
   */
  private JSONObject isJsonInvalid(String file) {
    try {

      JSONObject loadJsonObject = new JSONObject(readLineByLineJava8(file));
      if (!loadJsonObject.has("rightPane")) {
        return null;
      }
      if (!loadJsonObject.has("toolBar")) {
        return null;
      }

      return loadJsonObject;
    } catch (JSONException e) {
      return null;
    }


  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean startLoad(File file, Controller controller) {
    JSONObject loadedFile = isJsonInvalid(file.toString());
    if (loadedFile == null) {
      return false;
    }
    try {
      this.controller = controller;
      rightPane = loadedFile.getJSONArray("rightPane");
      toolBar = loadedFile.getJSONArray("toolBar");
    } catch (JSONException error) {
      return false;
    }
    return true;
  }


  /**
   * Read the data in the file corresponding to the Rectangle shape to restore it.
   * @param jsonObject the backup file to read
   * @return A new polygon with the properties saved in the file
   */
  public Rectangle loadRectangle(JSONObject jsonObject) {
    try {

      Position newPos = new Position(
          jsonObject.getJSONObject("position").getDouble("x"),
          jsonObject.getJSONObject("position").getDouble("y"));
      Color newCol = new Color(jsonObject.getJSONObject("color").getInt("r"),
          jsonObject.getJSONObject("color").getInt("g"),
          jsonObject.getJSONObject("color").getInt("b"));

      double height = jsonObject.getDouble("height");
      double width = jsonObject.getDouble("width");
      String name = jsonObject.getString("name");
      double angle = jsonObject.getDouble("angle");
      double reductionCoefficient = jsonObject.getDouble("reductionCoefficient");
      double virtualHeight = jsonObject.getDouble("virtualHeight");
      double virtualWidth = jsonObject.getDouble("virtualWidth");
      Rectangle rec = new Rectangle(name, newPos, newCol, width, height);
      rec.rotate(angle);
      rec.setReductionCoefficient(reductionCoefficient);
      rec.setVirtualHeight(virtualHeight);
      rec.setVirtualWidth(virtualWidth);

      return rec;
    } catch (JSONException error) {
      return null;
    }
  }

  /**
   * Read the data in the file corresponding to the Polygon shape to restore it.
   * @param jsonObject the backup file to read
   * @return A new polygon with the properties saved in the file
   */
  public Polygon loadPolygon(JSONObject jsonObject) {
    try {

      Position newPos = new Position(
          jsonObject.getJSONObject("position").getDouble("x"),
          jsonObject.getJSONObject("position").getDouble("y"));
      Color newCol = new Color(jsonObject.getJSONObject("color").getInt("r"),
          jsonObject.getJSONObject("color").getInt("g"),
          jsonObject.getJSONObject("color").getInt("b"));

      int numberOfSides = jsonObject.getInt("numberOfSides");
      double width = jsonObject.getDouble("width");
      String name = jsonObject.getString("name");
      double angle = jsonObject.getDouble("angle");
      double reductionCoefficient = jsonObject.getDouble("reductionCoefficient");
      double virtualWidth = jsonObject.getDouble("virtualWidth");
      Polygon polygon = new Polygon(name, newPos, newCol, numberOfSides, width);
      polygon.setReductionCoefficient(reductionCoefficient);
      polygon.setVirtualWidth(virtualWidth);
      polygon.rotate(angle);
      return polygon;
    } catch (JSONException error) {
      return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void loadRightPaneShapes() {
    try {
      for (int i = 0; i < rightPane.length(); i++) {
        JSONObject jsonObject = rightPane.getJSONObject(i);
        if (jsonObject.getString("name").equalsIgnoreCase("Rectangle")) {
          Rectangle rec = loadRectangle(jsonObject);
          controller.drawRectangle(rec);
        }
        else if (jsonObject.getString("name").equalsIgnoreCase("Polygon")) {
          Polygon polygon = loadPolygon(jsonObject);
          controller.drawPolygon(polygon);
        }
        else{
          GroupShape groupShape = loadGroupShape(jsonObject);
          groupShape.draw(controller);
        }
      }
    } catch (JSONException error) {
      error.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void loadToolBar(ToolBar toolBarImpl) {
    try {

      for (int i = 0; i < toolBar.length(); i++) {
        JSONObject jsonObject = toolBar.getJSONObject(i);
        if (jsonObject.getString("name").equalsIgnoreCase("Rectangle")) {
          Rectangle rec = loadRectangle(jsonObject);
          toolBarImpl.addShape(rec);
        }

        else if (jsonObject.getString("name").equalsIgnoreCase("Polygon")) {
          Polygon polygon = loadPolygon(jsonObject);
          toolBarImpl.addShape(polygon);
        }

        else {
          GroupShape groupShape = loadGroupShape(jsonObject);

          toolBarImpl.addShape(groupShape);
        }

        controller.refreshWindow();

      }
    } catch (JSONException error) {
      error.printStackTrace();
    }
  }



  /**
   * Read the data in the file corresponding to the GroupShape to restore it.
   * @param jsonObject the backup file to read
   * @return A new polygon with the properties saved in the file
   */
  private GroupShape loadGroupShape(JSONObject jsonObject){
    try {
      String name = jsonObject.getString("name");
      
      GroupShape group = new GroupShape(name);

      JSONArray children = jsonObject.getJSONArray("children");
      JSONArray virtualPositions = jsonObject.getJSONArray("virtualPositions");
      ArrayList<Position> positions = new ArrayList<Position>();
      for( int i = 0 ; i < virtualPositions.length(); i++){
        double x = virtualPositions.getJSONObject(i).getDouble("x");
        double y = virtualPositions.getJSONObject(i).getDouble("y");
        positions.add(new Position(x,y));
      }
      group.setVirtualChildrenPositions(positions);
      for(int i = 0; i < children.length(); i++){
        JSONObject child = children.getJSONObject(i);
        if(child.getString("name").equalsIgnoreCase("Rectangle")){
          Rectangle rectangle = loadRectangle(child);
          group.addShape(rectangle);
        }
        else if(child.getString("name").equalsIgnoreCase("Polygon")){
          Polygon polygon = loadPolygon(child);
          group.addShape(polygon);
        }
        else{
          GroupShape newGroup = loadGroupShape(child);
          group.addShape(newGroup);
        }
      }
      return group;
    } catch (JSONException error) {
      return null;
    }
  }

}
