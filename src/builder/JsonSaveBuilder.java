package builder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import shapeImplementation.SaveBuilder;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;
import visitor.JsonExportVisitor;

/**
 * Class for serializing a document in Json format
 */
public class JsonSaveBuilder implements SaveBuilder {

  JSONObject savedDraw;
  JSONArray rightPane;
  JSONArray toolBar;
  JsonExportVisitor jsonExport;

  /**
   * {@inheritDoc}
   */
  @Override
  public void startSave() {
    savedDraw = new JSONObject();
    rightPane = new JSONArray();
    toolBar = new JSONArray();
    jsonExport = new JsonExportVisitor();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveRightPaneShapes(ArrayList<Shape> drawingAreaShapes) {
    for (Shape s : drawingAreaShapes) {
      s.accept(jsonExport);
      rightPane.put(jsonExport.getJSONRepresentation());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveToolBar(ToolBar toolBar) {
    for (Shape s : toolBar.getShapes()) {
      s.accept(jsonExport);
      this.toolBar.put(jsonExport.getJSONRepresentation());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void endSave(File file) {

    try {
      savedDraw.put("rightPane", rightPane);
      savedDraw.put("toolBar", toolBar);
    } catch (JSONException e1) {

      e1.printStackTrace();
    }

    try (FileWriter newFile = new FileWriter(file)) {
      newFile.write(savedDraw.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
