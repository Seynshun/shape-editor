package visitor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;
import shapeImplementation.Visitor;

public class JsonExportVisitor implements Visitor {
    JSONObject save;

    @Override
    public void visitPolygon(Polygon p) {
        try {
            save = new JSONObject();
            JSONObject position = new JSONObject();
            position.put("x", p.getPosition().getX());
            position.put("y", p.getPosition().getY());

            JSONObject color = new JSONObject();
            color.put("r", p.getColor().getR());
            color.put("g", p.getColor().getG());
            color.put("b", p.getColor().getB());

            JSONArray xPoints = new JSONArray();
            JSONArray yPoints = new JSONArray();

            Position[] coordinates = p.getCornerCoordinates();
            for (int i = 0; i < p.getNumberOfSides(); i++) {
                Position corner = coordinates[i];
                xPoints.put(corner.getX());
                yPoints.put(corner.getY());
            }

            save.put("width", p.getWidth());

            save.put("name", p.getName());
            save.put("position", position);
            save.put("color", color);
            save.put("numberOfSides", p.getNumberOfSides());
            save.put("angle", p.getAngle());
            save.put("reductionCoefficient",p.getReductionCoefficient());
            save.put("virtualWidth",p.getVirtualWidth());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void visitRectangle(Rectangle r) {
        save = new JSONObject();

        try {

            JSONObject position = new JSONObject();
            position.put("x", r.getPosition().getX());
            position.put("y", r.getPosition().getY());

            JSONObject color = new JSONObject();
            color.put("r", r.getColor().getR());
            color.put("g", r.getColor().getG());
            color.put("b", r.getColor().getB());

            save.put("width", r.getWidth());
            save.put("height", r.getHeight());
            save.put("name", r.getName());
            save.put("position", position);
            save.put("color", color);
            save.put("angle", r.getAngle());
            save.put("reductionCoefficient",r.getReductionCoefficient());
            save.put("virtualHeight",r.getVirtualHeight());
            save.put("virtualWidth",r.getVirtualWidth());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public JSONObject getJSONRepresentation() {
        return this.save;
    }

    @Override
    public void visitGroupShape(GroupShape gs) {
        save = new JSONObject();
        try {
            JSONObject position = new JSONObject();
            position.put("x", gs.getPosition().getX());
            position.put("y", gs.getPosition().getY());
            
            JSONArray children = new JSONArray();
            JSONArray virtualPositions = new JSONArray();
            JsonExportVisitor exportVisitor = new JsonExportVisitor();
            for(Shape s: gs.getShapes()){
                s.accept(exportVisitor);
                children.put(exportVisitor.getJSONRepresentation());
            }
            for(Position p : gs.getVirtualPositions()){
                JSONObject virtual = new JSONObject();
                virtual.put("x",p.getX());
                virtual.put("y",p.getY());
                virtualPositions.put(virtual);

            }
            save.put("virtualPositions", virtualPositions);
            save.put("name", gs.getName());
            save.put("position", position);
            save.put("children", children);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




}