package visitor;

import shapeImplementation.GroupShape;
import shapeImplementation.Polygon;
import shapeImplementation.Rectangle;
import shapeImplementation.Visitor;

public class HtmlExportVisitor implements Visitor {
    /**
     * Il faudra surement utiliser écriture et lecture dans un fichier
     * 
     */
    @Override
    public void visitPolygon(Polygon p) {
        // Export html pour un polygon

    }

    @Override
    public void visitRectangle(Rectangle r) {
        // Export html pour un rectangle
    }

    @Override
    public void visitGroupShape(GroupShape gs) {
        // TODO Auto-generated method stub

    }

}