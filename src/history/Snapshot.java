package history;

import java.util.ArrayList;
import shapeImplementation.Shape;

public class Snapshot {

    ArrayList<Shape> drawingPaneMap = new ArrayList<>();
    ArrayList<Shape> toolBarMap = new ArrayList<>();

    public Snapshot(ArrayList<Shape> drawingPaneMap,ArrayList<Shape> toolBarMap){
        this.drawingPaneMap = drawingPaneMap;
        this.toolBarMap = toolBarMap;
    }

    public ArrayList<Shape> getDrawingPaneMap() {
        return drawingPaneMap;
    }
    public ArrayList<Shape> getToolBarMap() {
        return toolBarMap;
    }

    public String toString(){
        String res = "State snapshot Composé de: \n ";
        res += "DrawingPane \n";
        for(Shape s: drawingPaneMap){
            res+= String.format("%s \n",s.toString() );
        }

        res += "ToolBar \n";
        for(Shape s: toolBarMap){
            res+= String.format("%s \n",s.toString() );
        }

        return res;
    }
}
