package editors;

import commands.ColorCommand;
import shapeImplementation.Color;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Editor;
import shapeImplementation.Polygon;
import shapeImplementation.Shape;

/**
 * Class representing the editor of a Polygon
 */
public class PolygonEditor implements Editor {

  double initialLength;
  double initialAngle;
  int initialNumberOfSides;
  boolean canCancel = true;
  Color initialColor;
  Polygon shape;

  private static PolygonEditor instance;

  public static PolygonEditor getInstance() {
    if (instance == null ) {
      instance = new PolygonEditor();
    }
    return instance;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void initialisation(Shape s) {

    this.shape = (Polygon) s;
    initialColor = shape.getColor();
    initialLength = shape.getWidth();
    initialNumberOfSides = shape.getNumberOfSides();
    initialAngle = shape.getAngle();
  }

  public Polygon getShape() {
    return shape;
  }

  /**
   * Applies the changes selected from the editor to the shape
   * @param controller The rendering engine
   * @param color The color to be applied to the shape
   * @param width The width to be applied to the shape
   * @param numberOfSides The numberOfSides to be applied to the shape
   * @param angle The angle to be applied to the shape
   */
  public void applyModification(Controller controller,Color color,double width, int numberOfSides, double angle){

    shape.setWidth(width);
    shape.setNumberOfSides(numberOfSides);
    shape.rotate(angle);
    Command colorCommand = new ColorCommand(shape,color,controller);
    colorCommand.execute();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelModification(Controller controller) {

    shape.setWidth(initialLength);
    shape.setNumberOfSides(initialNumberOfSides);
    shape.rotate(initialAngle);
    Command colorCommand = new ColorCommand(shape,initialColor,controller);
    colorCommand.execute();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Color getInitialColor() {
    return initialColor;
  }

  public double getInitialAngle() {
    return initialAngle;
  }

  public double getInitialLength() {
    return initialLength;
  }

  public int getInitialNumberOfSides() {
    return initialNumberOfSides;
  }

  public void setCanCancel(boolean canCancel) {
    this.canCancel = canCancel;
  }

  public boolean isCanCancel() {
    return canCancel;
  }

}
