package editors;


import commands.ColorCommand;
import shapeImplementation.Color;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Editor;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;

/**
 * Class representing the editor of a Rectangle
 */
public class RectangleEditor  implements Editor {
  double initialHeight;
  double initialWidth;
  double initialAngle;
  boolean canCancel = true;
  Color initialColor;
  Rectangle shape;

  private static RectangleEditor instance;

  public static RectangleEditor getInstance() {
    if (instance == null ) {
      instance = new RectangleEditor();
    }
    return instance;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void initialisation(Shape s) {

    this.shape = (Rectangle) s;
    initialColor = shape.getColor();
    initialWidth = shape.getWidth();
    initialHeight = shape.getHeight();
    initialAngle = shape.getAngle();
  }

  /**
   * {@inheritDoc}
   */
  public Rectangle getShape() {
    return shape;
  }

  /**
   * Applies the changes selected from the editor to the shape
   * @param controller The rendering engine
   * @param color The color to be applied to the shape
   * @param width The width to be applied to the shape
   * @param height The height to be applied to the shape
   * @param angle The angle to be applied to the shape
   */
  public void applyModification(Controller controller,Color color,double width, double height, double angle){

    shape.setWidth(width);
    shape.setHeight(height);
    shape.rotate(angle);
    Command colorCommand = new ColorCommand(shape,color,controller);
    colorCommand.execute();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelModification(Controller controller) {
    shape.setHeight(initialHeight);
    shape.setWidth(initialWidth);
    shape.rotate(initialAngle);
    Command colorCommand = new ColorCommand(shape,initialColor,controller);
    colorCommand.execute();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Color getInitialColor() {
    return initialColor;
  }

  public double getInitialHeight() {
    return initialHeight;
  }

  public double getInitialWidth() {
    return initialWidth;
  }

  public double getInitialAngle() {
    return initialAngle;
  }

  public void setCanCancel(boolean canCancel) {
    this.canCancel = canCancel;
  }

  public boolean isCanCancel() {
    return canCancel;
  }
}
