package editors;

import java.util.ArrayList;
import shapeImplementation.Color;
import shapeImplementation.Controller;
import shapeImplementation.Editor;
import shapeImplementation.GroupShape;
import shapeImplementation.Shape;

/**
 * Class representing the editor of GroupShape
 */
public class GroupEditor implements Editor {


  boolean canCancel = true;
  double initialAngle;
  double initialSize;
  Color initialColor;
  GroupShape shape;
  ArrayList<Color> childrenColors;


  private static GroupEditor instance;

  public static GroupEditor getInstance() {
    if (instance == null ) {
      instance = new GroupEditor();
    }
    return instance;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void initialisation(Shape s) {

    this.shape = (GroupShape) s;
    initialColor = shape.getColor();
    initialSize = 0;
    initialAngle = shape.getAngle();
    childrenColors = new ArrayList<Color>();
    for(Shape child: s.getShapes()){
      childrenColors.add(child.getColor());
    }
  }

  public GroupShape getShape() {
    return shape;
  }

  /**
   * Applies the changes selected from the editor to the GroupShape
   * @param controller The rendering engine
   * @param color The color to be applied to the shape
   * @param size The size to be applied to the shape
   * @param angle The angle to be applied to the shape
   */
  public void applyModification(Controller controller,Color color,double size, double angle){
    if(color!=null)
      shape.setColor(color);
    initialSize = 0;
    shape.setSize(size);
    shape.rotate(angle);
    shape.draw(controller);
  }


  /**
   * Validates new changes to the shape
   * @param controller The rendering engine
   * @param color The color to be applied to the shape
   * @param angle The angle to be applied to the shape
   */
  public void validateModification(Controller controller, Color color, double angle){
    if(color!=null)
      shape.setColor(color);
    shape.rotate(angle);
    shape.draw(controller);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelModification(Controller controller) {
    int i = 0;
    for(Shape child: shape.getShapes()){
      child.setColor(childrenColors.get(i));
      i++;
    }
    shape.rotate(initialAngle);
    shape.setCoordinates();
    shape.draw(controller);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Color getInitialColor() {
    return initialColor;
  }

  public double getInitialAngle() {
    return initialAngle;
  }

  public double getInitialSize() {
    return initialSize;
  }

  public void setCanCancel(boolean canCancel) {
    this.canCancel = canCancel;
  }

  public boolean isCanCancel() {
    return canCancel;
  }
}
