package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;

public class BackupCommand implements Command {

    private Canvas mainCanvas = Canvas.getInstance();
    
    @Override
    public void execute() {
        mainCanvas.createBackup(this);

    }

    @Override
    public String getName() {
        return "BackupCommand";
    }

}