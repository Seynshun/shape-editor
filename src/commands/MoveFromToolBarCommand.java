package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Position;
import shapeImplementation.Shape;

public class MoveFromToolBarCommand implements Command {

    private Shape state;
    private Position position;
    private Controller controller;
    private Canvas mainCanvas = Canvas.getInstance();

    public MoveFromToolBarCommand(Shape s, double x, double y,Controller controller){
        this.state = s;
        position = new Position(x,y);
        this.controller = controller;
    }

    @Override
    public void execute() {
        state.setPosition(position);
        state.draw(controller);
    }

    @Override
    public String getName() {
        return "MoveFromToolBarCommand";
    }
}