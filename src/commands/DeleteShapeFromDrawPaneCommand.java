package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Shape;

public class DeleteShapeFromDrawPaneCommand implements Command {

    private Canvas mainCanvas = Canvas.getInstance();
    private Shape shape;
    private Controller controller;
    public DeleteShapeFromDrawPaneCommand(Shape shape,Controller controller){
        this.shape = shape;
        this.controller = controller;
    }
    @Override
    public void execute() {
        controller.removeShapeFromDrawPane(shape);
    }

    @Override
    public String getName() {
        return "deleteCommand";
    }

}