package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;

public class RedoCommand implements Command {
  private Canvas mainCanvas = Canvas.getInstance();
  @Override
  public void execute() {
    mainCanvas.redo();
  }

  @Override
  public String getName() {
    return null;
  }
}

