package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;

public class UndoCommand implements Command {

  private Canvas mainCanvas = Canvas.getInstance();
  @Override
  public void execute() {
    mainCanvas.undo();
  }

  @Override
  public String getName() {
    return null;
  }
}
