package commands;


import shapeImplementation.Canvas;
import shapeImplementation.Color;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Shape;

public class ColorCommand implements Command {
    private Shape state;
    private Color color;
    private Canvas mainCanvas = Canvas.getInstance();
    private Controller controller;
    public ColorCommand(Shape s, Color color,Controller controller){
        this.state = s;
        this.color= color;
        this.controller = controller;
    }

    @Override
    public void execute() {
        state.setColor(color);
        state.draw(controller);
    }

    @Override
    public String getName() {
        return "ColorCommand";
    }
}
