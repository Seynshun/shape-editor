package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;

public class DeleteShapeFromToolBarCommand implements Command {

    private Canvas mainCanvas = Canvas.getInstance();
    private ToolBar toolBar;
    private Shape shape;
    private Controller controller;
    public DeleteShapeFromToolBarCommand(ToolBar toolbar, Shape shape,Controller controller){
        this.toolBar = toolbar;
        this.shape = shape;
        this.controller = controller;
    }
    @Override
    public void execute() {
        toolBar.removeShape(shape);
        controller.removeShapeFromToolBar(shape);
    }

    @Override
    public String getName() {
        return "deleteCommand";
    }

}