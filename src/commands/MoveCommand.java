package commands;

import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Controller;
import shapeImplementation.Position;
import shapeImplementation.Shape;

public class MoveCommand implements Command {

    private Shape state;
    private Position position;
    private Controller controller;
    private Canvas mainCanvas = Canvas.getInstance();

    public MoveCommand(Shape s, double x, double y,Controller controller){
        this.state = s;
        position = new Position(x,y);
        this.controller = controller;
    }

    public void moveTo(Position pos){
        this.position = pos;
    }
    @Override
    public void execute() {
        state.translate(position.getX(),position.getY());
        state.draw(controller);
    }

    @Override
    public String getName() {
        return "MoveCommand";
    }
}
