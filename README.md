# Shape Editor

Shape Editor using design patterns for Software architecture course project of Master 1 of the University of Bordeaux.

<p align="center">
<img alt="terminal" src="https://zupimages.net/up/20/36/q71v.gif"/>
</p>

# Requirements

 - JDK 11
 - JavaFX 11.0.1

# Features

 - Drag and Drop from toolbar
 
 - Drag and Drop to toolbar
 
 - Editing objects properties : An object editor is opened by right-clicking on a shape. You can change color and others parameters according to the shape.
 
 - Grouping : Several shapes can be group together, the editor changes the properties of all the shapes in the group. Ctrl + left click on the shapes you want to group and press "Group" button. A group can contain subgroups and so on.
 
 - Group dissociation : Dissociates from previously grouped forms.  Ctrl + left click on the shapes you want to dissociate and press "Ungroup" button.

 - Undo-Redo : Click on the left arrow to Undo and right arrow to redo.
 
 - Document serialization : You can save the document by clicking on File and Save. You can load saved document by clicking on File, Load.
 
 - Software status backup :  Toolbar shapes are automatically saved. When the software is open, the toolbar is identical to the last use.

# Used Designs Patterns

 - Factory
 - Builder
 - Composite
 - Memento
 - Command
 - Singleton
 - Prototype
 - Bridge
 - Iterator
 - Visitor
