import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import commands.BackupCommand;
import commands.ColorCommand;
import commands.DeleteShapeFromToolBarCommand;
import commands.MoveCommand;
import commands.MoveFromToolBarCommand;
import commands.RedoCommand;
import commands.UndoCommand;
import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import java.util.ArrayList;
import org.junit.Test;
import shapeImplementation.Canvas;
import shapeImplementation.Color;
import shapeImplementation.Command;
import shapeImplementation.Polygon;
import shapeImplementation.Position;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;

public class CommandTest {

  @Test
  public void testMoveCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    double initialX = rectangle.getPosition().getX();
    double initialY = rectangle.getPosition().getY();

    Command moveCommand = new MoveCommand(rectangle,15,25,null);

    try {
      moveCommand.execute();
    }catch (NullPointerException e){
      assertEquals((int)initialX+15,(int)rectangle.getPosition().getX());
      assertEquals((int)initialY+25,(int)rectangle.getPosition().getY());
    }
  }

  @Test
  public void testColorCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Color color = new Color(230,147,98);
    Command colorCommand = new ColorCommand(rectangle,color,null);
    try {
      colorCommand.execute();
    }catch (NullPointerException e){
      assertEquals(rectangle.getColor(),color);
    }

  }

  @Test
  public void testDeleteShapeFromToolBarCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");

    ToolBar toolBar = new ToolBar();
    toolBar.addShape(polygon);
    toolBar.addShape(rectangle);
    assertNotNull(toolBar);

    ArrayList<Shape> expectedResult = new ArrayList<>();
    expectedResult.add(polygon);

    Command deleteShapeFromToolBarCommand = new DeleteShapeFromToolBarCommand(toolBar,rectangle,null);
    try {
      deleteShapeFromToolBarCommand.execute();
    }catch (NullPointerException e){
      assertEquals(toolBar.getShapes(),expectedResult);
    }

  }

  @Test
  public void testMoveFromToolBarCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");

    Command moveFromToolBarCommand = new MoveFromToolBarCommand(rectangle,25,30,null);
    try {
      moveFromToolBarCommand.execute();
    }catch (NullPointerException e){
      assertEquals(rectangle.getPosition(),new Position(25,30));
    }

  }

  @Test
  public void testBackupCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");


    ToolBar toolBar = new ToolBar();
    toolBar.addShape(polygon);
    Canvas canvas = Canvas.getInstance();
    canvas.setToolBar(toolBar);
    canvas.addShapeToDrawingArea(rectangle);

    assertTrue(canvas.history.isEmpty());

    Command backupCommand = new BackupCommand();
    backupCommand.execute();

    assertFalse(canvas.history.isEmpty());
    assertFalse(canvas.history.get(0).getSnapshot().getDrawingPaneMap().isEmpty());
    assertFalse(canvas.history.get(0).getSnapshot().getToolBarMap().isEmpty());
  }

  @Test
  public void testUndoRedoCommand(){
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");


    ToolBar toolBar = new ToolBar();
    toolBar.addShape(polygon);
    Canvas canvas = Canvas.getInstance();
    canvas.drawingAreaShapes.clear();
    canvas.toolBar.clearToolBar();
    canvas.setVirtualSize(0);

    canvas.setToolBar(toolBar);
    canvas.addShapeToDrawingArea(rectangle);

    Command backupCommand = new BackupCommand();

    backupCommand.execute(); //first state

    canvas.addShapeToDrawingArea(polygon);
    toolBar.addShape(rectangle);

    backupCommand.execute(); //second state

    boolean isRectangle = false;
    boolean isPolygon = false;
    boolean isRectangleInToolbar = false;
    boolean isPolygonInToolbar = false;
    for (Shape s : canvas.drawingAreaShapes){
      if (s instanceof Rectangle) isRectangle = true;
      if (s instanceof Polygon) isPolygon = true;
    }
    for (Shape s : canvas.toolBar.getShapes()){
      if (s instanceof Rectangle) isRectangleInToolbar = true;
      if (s instanceof Polygon) isPolygonInToolbar = true;
    }
    assertTrue(isPolygon);
    assertTrue(isRectangle); //There's a rectangle and a polygon in the second state
    assertTrue(isPolygonInToolbar);
    assertTrue(isRectangleInToolbar); //Same for toolbar

    Command undoCommand = new UndoCommand();
    undoCommand.execute(); //Back to state one

    isRectangle = false;
    isPolygon = false;
    isRectangleInToolbar = false;
    isPolygonInToolbar = false;
    for (Shape s : canvas.drawingAreaShapes){
      if (s instanceof Rectangle) isRectangle = true;
      if (s instanceof Polygon) isPolygon = true;
    }
    for (Shape s : canvas.toolBar.getShapes()){
      if (s instanceof Rectangle) isRectangleInToolbar = true;
      if (s instanceof Polygon) isPolygonInToolbar = true;
    }
    assertFalse(isPolygon);
    assertTrue(isRectangle); // there's no polygon in state one.
    assertTrue(isPolygonInToolbar);
    assertFalse(isRectangleInToolbar);

    Command redoCommand = new RedoCommand();
    redoCommand.execute(); //Back to state two

    isRectangle = false;
    isPolygon = false;
    isRectangleInToolbar = false;
    isPolygonInToolbar = false;
    for (Shape s : canvas.drawingAreaShapes){
      if (s instanceof Rectangle) isRectangle = true;
      if (s instanceof Polygon) isPolygon = true;
    }
    for (Shape s : canvas.toolBar.getShapes()){
      if (s instanceof Rectangle) isRectangleInToolbar = true;
      if (s instanceof Polygon) isPolygonInToolbar = true;
    }
    assertTrue(isPolygon);
    assertTrue(isRectangle); //There's a rectangle and a polygon in the second state
    assertTrue(isPolygonInToolbar);
    assertTrue(isRectangleInToolbar); //Same for toolbar
  }

}
