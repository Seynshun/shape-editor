import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import org.junit.Test;
import shapeImplementation.Shape;

public class PrototypeTest {

  @Test
  public void testCloneShape(){

    AbstractShapeFactory shapeFactory = new ShapeFactory();

    Shape rectangle =shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");

    Shape clonedRectangle = rectangle.clone();
    Shape clonedPolygon = polygon.clone();

    //Check that they are not the same objects
    assertNotEquals(rectangle,clonedRectangle);
    assertNotEquals(polygon,clonedPolygon);

    //Check that they have the same values
    assertEquals(rectangle.getName(),clonedRectangle.getName());
    assertEquals(rectangle.getPosition(),clonedRectangle.getPosition());
    assertEquals((int)rectangle.getAngle(),(int)clonedRectangle.getAngle());
    assertEquals((int)rectangle.getSize(),(int)clonedRectangle.getSize());

  }

}
