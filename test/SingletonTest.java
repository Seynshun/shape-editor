import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import editors.GroupEditor;
import editors.PolygonEditor;
import editors.RectangleEditor;
import org.junit.Test;
import shapeImplementation.Canvas;


public class SingletonTest {

  @Test
  public void testSingletonCanvas(){

    Canvas canvas = Canvas.getInstance();
    assertNotNull(canvas);

    Canvas newCanvas = Canvas.getInstance();
    assertSame(canvas,newCanvas);

  }

  @Test
  public void testSingletonPolygonEditor(){

    PolygonEditor polygonEditor = PolygonEditor.getInstance();
    assertNotNull(polygonEditor);

    PolygonEditor newPolygonEditor = PolygonEditor.getInstance();
    assertSame(polygonEditor,newPolygonEditor);

  }

  @Test
  public void testSingletonRectangleEditor(){

    RectangleEditor rectangleEditor = RectangleEditor.getInstance();
    assertNotNull(rectangleEditor);

    RectangleEditor newRectangleEditor = RectangleEditor.getInstance();
    assertSame(rectangleEditor,newRectangleEditor);

  }

  @Test
  public void testSingletonGroupEditor(){

    GroupEditor groupEditor = GroupEditor.getInstance();
    assertNotNull(groupEditor);

    GroupEditor newGroupEditor = GroupEditor.getInstance();
    assertSame(groupEditor,newGroupEditor);

  }


}
