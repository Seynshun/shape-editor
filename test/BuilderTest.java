import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import java.io.File;
import java.io.IOException;
import junitx.framework.FileAssert;
import org.junit.Test;
import shapeImplementation.Canvas;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;

public class BuilderTest {

  @Test
  public void testSaveBuilder() throws IOException {

    File testBuilder = new File("test/files/testSaveBuilder.json");
    if (testBuilder.exists()) {
      testBuilder.delete();
    }
    testBuilder.createNewFile();

    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");
    ToolBar toolBar = new ToolBar();
    Canvas canvas = Canvas.getInstance();
    canvas.setToolBar(toolBar);
    canvas.toolBar.addShape(polygon);
    canvas.toolBar.addShape(rectangle);
    canvas.drawingAreaShapes.add(rectangle);
    canvas.drawingAreaShapes.add(polygon);

    canvas.saveFile(testBuilder);
    File expectedSaveBuilder = new File("test/files/expectedSaveBuilder.json");
    FileAssert.assertEquals(testBuilder, expectedSaveBuilder);

  }
}
