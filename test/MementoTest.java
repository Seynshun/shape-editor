import static org.junit.Assert.assertEquals;

import commands.BackupCommand;
import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import java.util.ArrayList;
import org.junit.Test;
import shapeImplementation.Canvas;
import shapeImplementation.Command;
import shapeImplementation.Shape;
import shapeImplementation.ToolBar;

public class MementoTest {

  @Test
  public void testMementoUndoRedo(){

    AbstractShapeFactory abstractShapeFactory = new ShapeFactory();
    Shape rectangle = abstractShapeFactory.getShape("rectangle");
    Shape polygon =  abstractShapeFactory.getShape("polygon");

    ArrayList<Shape> drawAreaState = new ArrayList<>();
    drawAreaState.add(polygon);

    ToolBar toolBar = new ToolBar();
    toolBar.addShape(rectangle);
    toolBar.addShape(polygon);

    Canvas canvas = Canvas.getInstance();
    canvas.drawingAreaShapes.clear();
    canvas.toolBar.clearToolBar();
    canvas.setVirtualSize(0);
    canvas.history.clear();

    canvas.setToolBar(toolBar);
    canvas.addShapeToDrawingArea(polygon);

    Command backupCommand = new BackupCommand();
    backupCommand.execute();

    assertEquals(canvas.history.size(),1); //only one backup

    drawAreaState.add(rectangle);
    backupCommand.execute();

    assertEquals(canvas.history.size(),2); //two backups
    assertEquals(canvas.getVirtualSize(),2); // We are in state 2

    canvas.undo();
    assertEquals(canvas.getVirtualSize(),1); //back to state 1

    canvas.undo();
    assertEquals(canvas.getVirtualSize(),1); // No more undo available so virtualSize stays the same

    canvas.redo();
    assertEquals(canvas.getVirtualSize(),2); // back to state 2

    canvas.redo();
    assertEquals(canvas.getVirtualSize(),2); // No more redo available so virtualSize stays the same

  }

}
