import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import org.junit.Assert;
import org.junit.Test;
import shapeImplementation.Polygon;
import shapeImplementation.Rectangle;
import shapeImplementation.Shape;

public class FactoryMethodTest {

  @Test
  public void testFactoryMethod(){

    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle = shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");

    Assert.assertTrue(rectangle instanceof Rectangle);
    Assert.assertTrue(polygon instanceof Polygon);

  }

}
