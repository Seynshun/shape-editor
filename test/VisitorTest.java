import static org.junit.Assert.assertEquals;

import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.Test;
import shapeImplementation.Shape;
import visitor.JsonExportVisitor;


public class VisitorTest {


  /**
   *
   * Test visitor pattern with JsonExport class
   *
   */
  @Test
  public void testJsonExportVisitor() {

    String expectedRectangleVisitor = null;
    String expectedPolygonVisitor = null;

    try{
      FileInputStream rectangleFile = new FileInputStream("test/files/expectedRectangleVisitor.json");
      FileInputStream polygonFile = new FileInputStream("test/files/expectedPolygonVisitor.json");
      expectedRectangleVisitor = IOUtils.toString(rectangleFile);
      expectedPolygonVisitor = IOUtils.toString(polygonFile);

    } catch (IOException e) {
      e.printStackTrace();
    }
    AbstractShapeFactory shapeFactory = new ShapeFactory();
    Shape rectangle =shapeFactory.getShape("rectangle");

    JsonExportVisitor jsonExport = new JsonExportVisitor();
    rectangle.accept(jsonExport);

    JSONObject result = jsonExport.getJSONRepresentation();
    assertEquals(result.toString(),expectedRectangleVisitor);

    Shape polygon = shapeFactory.getShape("polygon");
    polygon.accept(jsonExport);
    result = jsonExport.getJSONRepresentation();
    assertEquals(result.toString(), expectedPolygonVisitor);

  }

}
