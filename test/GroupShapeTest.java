import static org.junit.Assert.assertEquals;

import factory.AbstractShapeFactory;
import factory.ShapeFactory;
import java.util.ArrayList;
import org.junit.Test;
import shapeImplementation.GroupShape;
import shapeImplementation.Shape;

public class GroupShapeTest {


  /**
   * Test composite pattern with GroupShape class
   */
  @Test
  public void testGroupShape(){

    AbstractShapeFactory shapeFactory = new ShapeFactory();

    Shape rectangle =shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");

    Shape groupShape = new GroupShape("name");
    Shape subGroup = new GroupShape("name");

    groupShape.addShape(rectangle);
    groupShape.addShape(polygon);

    subGroup.addShape(rectangle);
    subGroup.addShape(polygon);

    groupShape.addShape(subGroup);

    assertEquals(rectangle,groupShape.getShapes().get(0));
    assertEquals(polygon,groupShape.getShapes().get(1));
    assertEquals(subGroup,groupShape.getShapes().get(2));
    assertEquals(rectangle,groupShape.getShapes().get(2).getShapes().get(0));
    assertEquals(polygon,groupShape.getShapes().get(2).getShapes().get(1));

  }

  /**
   * Test iterator pattern with GroupShape class
   */
  @Test
  public void testIterator(){

    AbstractShapeFactory shapeFactory = new ShapeFactory();

    ArrayList<Shape> result = new ArrayList<>();

    Shape rectangle =shapeFactory.getShape("rectangle");
    Shape polygon = shapeFactory.getShape("polygon");

    Shape groupShape = new GroupShape("name");
    Shape subGroup = new GroupShape("name");

    groupShape.addShape(rectangle);
    groupShape.addShape(polygon);

    subGroup.addShape(rectangle);
    subGroup.addShape(polygon);

    groupShape.addShape(subGroup);

    groupShape.findAllShapes(result);
    assertEquals(rectangle,result.get(0));
    assertEquals(polygon,result.get(1));
    assertEquals(rectangle,result.get(2));
    assertEquals(polygon,result.get(3));

  }

}
